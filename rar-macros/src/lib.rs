extern crate proc_macro;

use darling::ast::Data::Struct;
use darling::{ast, util, FromDeriveInput, FromField};
use proc_macro::TokenStream;
use quote::quote;
use syn::{parse_macro_input, DeriveInput, Ident};

#[derive(Debug, FromField)]
#[darling(attributes(register))]
struct RegisterField {
    ident: Option<Ident>,
    address: u32,
    #[darling(default)]
    reset: Option<u32>,
    #[darling(default)]
    write_mask: Option<u32>,
    #[darling(default)]
    read_mask: Option<u32>,
}

#[derive(Debug, FromDeriveInput)]
#[darling(attributes(register), supports(struct_named))]
struct MemoryMappedDeviceArgs {
    ident: Ident,
    data: ast::Data<util::Ignored, RegisterField>,
}

/// Implements the MemoryMappedDevice trait of the rust-arm crate for the
/// given struct with named fields.
///
/// This derive requires at least one field marked with:
/// \#\[register\(address=\<address>, \[writable=true|false])
///
#[proc_macro_derive(MemoryMappedDevice, attributes(register))]
pub fn memory_mapped_device_derive(input: TokenStream) -> TokenStream {
    let parsed = parse_macro_input!(input as DeriveInput);
    let args: MemoryMappedDeviceArgs = MemoryMappedDeviceArgs::from_derive_input(&parsed).unwrap();

    let fields = match args.data {
        Struct(ref fields) => fields,
        // todo: what is the correct handling of panics in a proc macro?
        _ => unreachable!(),
    };

    let reset_values = fields
        .iter()
        .map(|r_args| r_args.reset.unwrap_or(0))
        .collect::<Vec<_>>();

    let ident = &args.ident;

    let all_field_idents = fields
        .fields
        .iter()
        .map(|r_args| &r_args.ident)
        .collect::<Vec<_>>();
    let addresses = fields
        .fields
        .iter()
        .map(|r_args| &r_args.address)
        .collect::<Vec<_>>();

    let writable_fields = fields
        .fields
        .iter()
        .filter(|r_args| r_args.write_mask.unwrap_or(0x0) != 0x0)
        .collect::<Vec<_>>();
    let writable_field_idents = writable_fields
        .iter()
        .map(|r_args| &r_args.ident)
        .collect::<Vec<_>>();
    let writable_addresses = writable_fields
        .iter()
        .map(|r_args| &r_args.address)
        .collect::<Vec<_>>();

    let read_masks = fields
        .iter()
        .map(|r_args| r_args.read_mask.unwrap_or(u32::MAX))
        .collect::<Vec<_>>();
    let write_masks = writable_fields
        .iter()
        .map(|r_args| r_args.write_mask.unwrap_or(0x0))
        .collect::<Vec<_>>();

    impl_memory_mapped_device(
        ident,
        &all_field_idents,
        &addresses,
        &writable_field_idents,
        &writable_addresses,
        &read_masks,
        &write_masks,
        &reset_values,
    )
}

// todo: fix error handling after dir restructuring and fix tests

fn impl_memory_mapped_device(
    struct_ident: &Ident,
    field_idents: &Vec<&Option<Ident>>,
    addresses: &Vec<&u32>,
    writable_field_idents: &Vec<&Option<Ident>>,
    writable_addresses: &Vec<&u32>,
    read_masks: &Vec<u32>,
    write_masks: &Vec<u32>,
    reset_values: &Vec<u32>,
) -> TokenStream {
    // workaround to get this to work in the rar-lib/ rusty-arm crate and in
    // other crates
    let crate_or_pkg = if std::env::var("CARGO_PKG_NAME").unwrap() == "rusty-arm" {
        quote! { crate }
    } else {
        quote! { rusty_arm }
    };

    let is_mapped_expr = if addresses.is_empty() {
        quote! { false }
    } else {
        quote! { #(address == #addresses)||* }
    };

    let read_expr = if addresses.is_empty() {
        quote! {
            Err(#crate_or_pkg::peripheral::error::PeripheralError::new_read(
                    #crate_or_pkg::peripheral::error::ErrorKind::UnhandledAddress,
                    address,
                    #crate_or_pkg::peripheral::error::ErrorOrigin::Device(
                        std::any::type_name::<#struct_ident>().to_string()),
            ))
        }
    } else {
        quote! {
                Ok(match address {
                    #(#addresses => self.#field_idents & #read_masks,)*
                    _ => unreachable!(),
                })
        }
    };

    let is_writable_expr = if writable_addresses.is_empty() {
        quote! { false }
    } else {
        quote! { #(address == #writable_addresses)||* }
    };

    let write_expr = if writable_addresses.is_empty() {
        quote! {
            Err(#crate_or_pkg::peripheral::error::PeripheralError::new_write(
                    #crate_or_pkg::peripheral::error::ErrorKind::UnhandledAddress,
                    address,
                    #crate_or_pkg::peripheral::error::ErrorOrigin::Device(
                        std::any::type_name::<#struct_ident>().to_string()),
            ))
        }
    } else {
        quote! {
            Ok(match address {
                #(#writable_addresses => self.#writable_field_idents = value & #write_masks,)*
                _ => unreachable!(),
            })
        }
    };

    let output = quote! {
        impl MemoryMappedDevice for #struct_ident {
            fn is_mapped(&self, address: u32) -> bool {
                return #is_mapped_expr
            }

            fn read(&self, address: u32) -> Result<u32, #crate_or_pkg::peripheral::error::PeripheralError> {
                if !self.is_mapped(address) {
                    Err(#crate_or_pkg::peripheral::error::PeripheralError::new_read(
                            #crate_or_pkg::peripheral::error::ErrorKind::UnhandledAddress,
                            address,
                            #crate_or_pkg::peripheral::error::ErrorOrigin::Device(
                                std::any::type_name::<#struct_ident>().to_string()),
                    ))
                } else {
                    #read_expr
                }
            }

            fn write(&mut self, address: u32, value: u32) -> Result<(), #crate_or_pkg::peripheral::error::PeripheralError>{
                fn is_writable(address: u32) -> bool {
                    return #is_writable_expr
                }

                if !self.is_mapped(address) || !is_writable(address) {
                    Err(#crate_or_pkg::peripheral::error::PeripheralError::new_write(
                            #crate_or_pkg::peripheral::error::ErrorKind::UnhandledAddress,
                            address,
                            #crate_or_pkg::peripheral::error::ErrorOrigin::Device(
                                std::any::type_name::<#struct_ident>().to_string()),
                    ))
                } else {
                    #write_expr
                }
            }

            fn reset(&mut self) {
                #(self.#field_idents = #reset_values;)*
            }
        }
    };

    proc_macro::TokenStream::from(output)
}
