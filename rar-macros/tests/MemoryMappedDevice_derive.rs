// todo: check if this can be replaced with the std version, which is
//  currently in development, at some point. see:
//  - https://github.com/rust-lang/rust/issues/82775
//  - https://doc.rust-lang.org/nightly/std/assert_matches/macro.assert_matches.html
#[macro_use]
extern crate assert_matches;

#[allow(non_snake_case)]
#[cfg(test)]
mod tests {
    use rusty_arm::peripheral::error::{ErrorKind, ErrorOrigin, Operation, PeripheralError};
    use rusty_arm::peripheral::MemoryMappedDevice;

    use rusty_arm_macro::MemoryMappedDevice;

    #[derive(MemoryMappedDevice, Debug)]
    struct ReadOnlyRegDev {
        #[register(address = 0x500)]
        reg: u32,
    }

    #[derive(MemoryMappedDevice, Debug)]
    struct WritableRegDev {
        #[register(address = 0x500, write_mask = 0xFFFFFFFF)]
        reg: u32,
    }

    #[derive(MemoryMappedDevice, Debug)]
    struct MixedMultiRegDev {
        #[register(address = 0x500, write_mask = 0x0)]
        reg_0: u32,
        #[register(address = 0x504, write_mask = 0xFFFFFFFF)]
        reg_1: u32,
    }

    // region ReadOnlyRegDev
    #[test]
    fn ReadOnlyRegDev_read_at_0x500_succeeds() {
        let dev = ReadOnlyRegDev { reg: 100 };

        let result: Result<u32, PeripheralError> = dev.read(0x500);

        assert!(result.is_ok());
        assert_eq!(result.unwrap(), 100);
    }

    #[test]
    fn ReadOnlyRegDev_read_at_0x504_fails() {
        let dev = ReadOnlyRegDev { reg: 100 };

        let result: Result<u32, PeripheralError> = dev.read(0x504);

        assert!(result.is_err());
        let error = result.unwrap_err();

        assert_eq!(*error.operation(), Operation::Read);
        assert_matches!(error.kind(), ErrorKind::UnhandledAddress);
        assert_eq!(error.address(), 0x504);
        assert_matches!(error.origin(), ErrorOrigin::Device(ref name) => {
            assert_eq!(name, std::any::type_name::<ReadOnlyRegDev>());
        });
    }

    #[test]
    fn ReadOnlyRegDev_write_at_0x500_fails() {
        let mut dev = ReadOnlyRegDev { reg: 100 };

        let result: Result<(), PeripheralError> = dev.write(0x500, 200);

        assert!(result.is_err());
        let error = result.unwrap_err();

        assert_eq!(*error.operation(), Operation::Write);
        assert_matches!(error.kind(), ErrorKind::UnhandledAddress);
        assert_eq!(error.address(), 0x500);
        assert_matches!(error.origin(), ErrorOrigin::Device(ref name) => {
            assert_eq!(name, std::any::type_name::<ReadOnlyRegDev>());
        });
    }

    #[test]
    fn ReadOnlyRegDev_write_at_0x504_fails() {
        let mut dev = ReadOnlyRegDev { reg: 100 };

        let result: Result<(), PeripheralError> = dev.write(0x504, 200);

        assert!(result.is_err());
        let error = result.unwrap_err();

        assert_eq!(*error.operation(), Operation::Write);
        assert_matches!(error.kind(), ErrorKind::UnhandledAddress);
        assert_eq!(error.address(), 0x504);
        assert_matches!(error.origin(), ErrorOrigin::Device(ref name) => {
            assert_eq!(name, std::any::type_name::<ReadOnlyRegDev>());
        });
    }
    // endregion

    // region WritableRegDev
    #[test]
    fn WritableRegDev_read_at_0x500_succeeds() {
        let dev = WritableRegDev { reg: 100 };

        let result: Result<u32, PeripheralError> = dev.read(0x500);

        assert!(result.is_ok());
        assert_eq!(result.unwrap(), 100);
    }

    #[test]
    fn WritableRegDev_read_at_0x504_fails() {
        let dev = WritableRegDev { reg: 100 };

        let result: Result<u32, PeripheralError> = dev.read(0x504);

        assert!(result.is_err());
        let error = result.unwrap_err();

        assert_eq!(*error.operation(), Operation::Read);
        assert_matches!(error.kind(), ErrorKind::UnhandledAddress);
        assert_eq!(error.address(), 0x504);
        assert_matches!(error.origin(), ErrorOrigin::Device(ref name) => {
            assert_eq!(name, std::any::type_name::<WritableRegDev>());
        });
    }

    #[test]
    fn WritableRegDev_write_at_0x500_succeeds() {
        let mut dev = WritableRegDev { reg: 100 };

        let result: Result<(), PeripheralError> = dev.write(0x500, 200);

        assert!(result.is_ok());
    }

    #[test]
    fn WritableRegDev_write_at_0x504_fails() {
        let mut dev = WritableRegDev { reg: 100 };

        let result: Result<(), PeripheralError> = dev.write(0x504, 200);

        assert!(result.is_err());
        let error = result.unwrap_err();

        assert_eq!(*error.operation(), Operation::Write);
        assert_matches!(error.kind(), ErrorKind::UnhandledAddress);
        assert_eq!(error.address(), 0x504);
        assert_matches!(error.origin(), ErrorOrigin::Device(ref name) => {
            assert_eq!(name, std::any::type_name::<WritableRegDev>());
        });
    }
    // endregion

    // region MixedMultiRegDev
    #[test]
    fn MixedMultiRegDev_read_at_0x500_succeeds() {
        let dev = MixedMultiRegDev {
            reg_0: 100,
            reg_1: 200,
        };

        let result: Result<u32, PeripheralError> = dev.read(0x500);

        assert!(result.is_ok());
        assert_eq!(result.unwrap(), 100);
    }

    #[test]
    fn MixedMultiRegDev_read_at_0x504_succeeds() {
        let dev = MixedMultiRegDev {
            reg_0: 100,
            reg_1: 200,
        };

        let result: Result<u32, PeripheralError> = dev.read(0x504);

        assert!(result.is_ok());
        assert_eq!(result.unwrap(), 200);
    }

    #[test]
    fn MixedMultiRegDev_read_at_0x508_fails() {
        let dev = MixedMultiRegDev {
            reg_0: 100,
            reg_1: 200,
        };

        let result: Result<u32, PeripheralError> = dev.read(0x508);

        assert!(result.is_err());
        let error = result.unwrap_err();

        assert_eq!(*error.operation(), Operation::Read);
        assert_matches!(error.kind(), ErrorKind::UnhandledAddress);
        assert_eq!(error.address(), 0x508);
        assert_matches!(error.origin(), ErrorOrigin::Device(ref name) => {
            assert_eq!(name, std::any::type_name::<MixedMultiRegDev>());
        });
    }

    #[test]
    fn MixedMultiRegDev_write_at_0x500_fails() {
        let mut dev = MixedMultiRegDev {
            reg_0: 100,
            reg_1: 200,
        };

        let result: Result<(), PeripheralError> = dev.write(0x500, 200);

        assert!(result.is_err());
        let error = result.unwrap_err();

        assert_eq!(*error.operation(), Operation::Write);
        assert_matches!(error.kind(), ErrorKind::UnhandledAddress);
        assert_eq!(error.address(), 0x500);
        assert_matches!(error.origin(), ErrorOrigin::Device(ref name) => {
            assert_eq!(name, std::any::type_name::<MixedMultiRegDev>());
        });
    }

    #[test]
    fn MixedMultiRegDev_write_at_0x504_succeeds() {
        let mut dev = MixedMultiRegDev {
            reg_0: 100,
            reg_1: 200,
        };

        let result: Result<(), PeripheralError> = dev.write(0x504, 300);
        assert!(result.is_ok());
    }
    // endregion
}
