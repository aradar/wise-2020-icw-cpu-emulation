use crate::{Memory, MemoryMappedDevice};
use std::fs::File;
use std::io::Read;
use std::path::Path;

pub fn set_bit_at_pos(value: u32, pos: u32, bit_value: bool) -> u32 {
    if bit_value {
        value | (1u32 << pos)
    } else {
        value & !(1u32 << pos)
    }
}

pub fn get_bit_at_pos(value: u32, pos: u32) -> bool {
    ((value >> pos) & 1) == 1
}

pub fn set_bits_at_pos(value: u32, pos: u32, bits: u32, num_bits: u32) -> u32 {
    let mask = ((1u32 << num_bits) - 1u32) << pos;
    (value & !mask) | (bits << pos)
}

pub fn get_bits_at_pos(value: u32, pos: u32, num_bits: u32) -> u32 {
    let mask = ((1u32 << num_bits) - 1u32) << pos;
    (value & mask) >> pos
}

pub fn load_program(pc: u32, program_file: &Path, memory: &mut Memory) {
    let mut buffer = Vec::new();

    let mut f = File::open(program_file).expect("Program file cannot be opened!");
    f.read_to_end(&mut buffer)
        .expect("Loading of the program file failed!");

    for i in 0..buffer.len() {
        memory.write_u8(pc + i as u32, buffer[i]).unwrap();
    }
}

#[cfg(test)]
mod tests {
    use super::{get_bit_at_pos, get_bits_at_pos, set_bit_at_pos, set_bits_at_pos};

    #[test]
    fn get_bit_at_pos_with_0_in_all_positions() {
        assert!(!get_bit_at_pos(0b00000000_00000000_00000000_00000000, 0));
        assert!(!get_bit_at_pos(0b00000000_00000000_00000000_00000000, 1));
        assert!(!get_bit_at_pos(0b00000000_00000000_00000000_00000000, 2));
        assert!(!get_bit_at_pos(0b00000000_00000000_00000000_00000000, 3));
        assert!(!get_bit_at_pos(0b00000000_00000000_00000000_00000000, 4));
        assert!(!get_bit_at_pos(0b00000000_00000000_00000000_00000000, 5));
        assert!(!get_bit_at_pos(0b00000000_00000000_00000000_00000000, 6));
        assert!(!get_bit_at_pos(0b00000000_00000000_00000000_00000000, 7));
        assert!(!get_bit_at_pos(0b00000000_00000000_00000000_00000000, 8));
        assert!(!get_bit_at_pos(0b00000000_00000000_00000000_00000000, 9));
        assert!(!get_bit_at_pos(0b00000000_00000000_00000000_00000000, 10));
        assert!(!get_bit_at_pos(0b00000000_00000000_00000000_00000000, 11));
        assert!(!get_bit_at_pos(0b00000000_00000000_00000000_00000000, 12));
        assert!(!get_bit_at_pos(0b00000000_00000000_00000000_00000000, 13));
        assert!(!get_bit_at_pos(0b00000000_00000000_00000000_00000000, 14));
        assert!(!get_bit_at_pos(0b00000000_00000000_00000000_00000000, 15));
        assert!(!get_bit_at_pos(0b00000000_00000000_00000000_00000000, 16));
        assert!(!get_bit_at_pos(0b00000000_00000000_00000000_00000000, 17));
        assert!(!get_bit_at_pos(0b00000000_00000000_00000000_00000000, 18));
        assert!(!get_bit_at_pos(0b00000000_00000000_00000000_00000000, 19));
        assert!(!get_bit_at_pos(0b00000000_00000000_00000000_00000000, 20));
        assert!(!get_bit_at_pos(0b00000000_00000000_00000000_00000000, 21));
        assert!(!get_bit_at_pos(0b00000000_00000000_00000000_00000000, 22));
        assert!(!get_bit_at_pos(0b00000000_00000000_00000000_00000000, 23));
        assert!(!get_bit_at_pos(0b00000000_00000000_00000000_00000000, 24));
        assert!(!get_bit_at_pos(0b00000000_00000000_00000000_00000000, 25));
        assert!(!get_bit_at_pos(0b00000000_00000000_00000000_00000000, 26));
        assert!(!get_bit_at_pos(0b00000000_00000000_00000000_00000000, 27));
        assert!(!get_bit_at_pos(0b00000000_00000000_00000000_00000000, 28));
        assert!(!get_bit_at_pos(0b00000000_00000000_00000000_00000000, 29));
        assert!(!get_bit_at_pos(0b00000000_00000000_00000000_00000000, 30));
        assert!(!get_bit_at_pos(0b00000000_00000000_00000000_00000000, 31));
    }

    #[test]
    fn get_bit_at_pos_with_1_in_all_positions() {
        assert!(get_bit_at_pos(0b11111111_11111111_11111111_11111111, 0));
        assert!(get_bit_at_pos(0b11111111_11111111_11111111_11111111, 1));
        assert!(get_bit_at_pos(0b11111111_11111111_11111111_11111111, 2));
        assert!(get_bit_at_pos(0b11111111_11111111_11111111_11111111, 3));
        assert!(get_bit_at_pos(0b11111111_11111111_11111111_11111111, 4));
        assert!(get_bit_at_pos(0b11111111_11111111_11111111_11111111, 5));
        assert!(get_bit_at_pos(0b11111111_11111111_11111111_11111111, 6));
        assert!(get_bit_at_pos(0b11111111_11111111_11111111_11111111, 7));
        assert!(get_bit_at_pos(0b11111111_11111111_11111111_11111111, 8));
        assert!(get_bit_at_pos(0b11111111_11111111_11111111_11111111, 9));
        assert!(get_bit_at_pos(0b11111111_11111111_11111111_11111111, 10));
        assert!(get_bit_at_pos(0b11111111_11111111_11111111_11111111, 11));
        assert!(get_bit_at_pos(0b11111111_11111111_11111111_11111111, 12));
        assert!(get_bit_at_pos(0b11111111_11111111_11111111_11111111, 13));
        assert!(get_bit_at_pos(0b11111111_11111111_11111111_11111111, 14));
        assert!(get_bit_at_pos(0b11111111_11111111_11111111_11111111, 15));
        assert!(get_bit_at_pos(0b11111111_11111111_11111111_11111111, 16));
        assert!(get_bit_at_pos(0b11111111_11111111_11111111_11111111, 17));
        assert!(get_bit_at_pos(0b11111111_11111111_11111111_11111111, 18));
        assert!(get_bit_at_pos(0b11111111_11111111_11111111_11111111, 19));
        assert!(get_bit_at_pos(0b11111111_11111111_11111111_11111111, 20));
        assert!(get_bit_at_pos(0b11111111_11111111_11111111_11111111, 21));
        assert!(get_bit_at_pos(0b11111111_11111111_11111111_11111111, 22));
        assert!(get_bit_at_pos(0b11111111_11111111_11111111_11111111, 23));
        assert!(get_bit_at_pos(0b11111111_11111111_11111111_11111111, 24));
        assert!(get_bit_at_pos(0b11111111_11111111_11111111_11111111, 25));
        assert!(get_bit_at_pos(0b11111111_11111111_11111111_11111111, 26));
        assert!(get_bit_at_pos(0b11111111_11111111_11111111_11111111, 27));
        assert!(get_bit_at_pos(0b11111111_11111111_11111111_11111111, 28));
        assert!(get_bit_at_pos(0b11111111_11111111_11111111_11111111, 29));
        assert!(get_bit_at_pos(0b11111111_11111111_11111111_11111111, 30));
        assert!(get_bit_at_pos(0b11111111_11111111_11111111_11111111, 31));
    }

    #[test]
    fn get_bits_at_pos_0() {
        assert_eq!(
            0b00000000_00000000_00000000_00000111,
            get_bits_at_pos(0b00000000_00000000_00000000_00000111, 0, 3)
        );
        assert_eq!(
            0b00000000_00000000_00000000_00000110,
            get_bits_at_pos(0b00000000_00000000_00000000_00001110, 0, 3)
        );
        assert_eq!(
            0b00000000_00000000_00000000_00000100,
            get_bits_at_pos(0b00000000_00000000_00000000_00011100, 0, 3)
        );
        assert_eq!(
            0b00000000_00000000_00000000_00000000,
            get_bits_at_pos(0b00000000_00000000_00000000_00111000, 0, 3)
        );
    }

    #[test]
    fn get_bits_at_pos_8() {
        assert_eq!(
            0b00000000_00000000_00000000_00000111,
            get_bits_at_pos(0b00000000_00000000_00000111_00000000, 8, 3)
        );
        assert_eq!(
            0b00000000_00000000_00000000_00000110,
            get_bits_at_pos(0b00000000_00000000_00001110_00000000, 8, 3)
        );
        assert_eq!(
            0b00000000_00000000_00000000_00000100,
            get_bits_at_pos(0b00000000_00000000_00011100_00000000, 8, 3)
        );
        assert_eq!(
            0b00000000_00000000_00000000_00000000,
            get_bits_at_pos(0b00000000_00000000_00111000_00000000, 8, 3)
        );
    }

    #[test]
    fn set_bits_at_pos_with_0_as_value() {
        assert_eq!(0b101, set_bits_at_pos(0, 0, 0b101, 3));
        assert_eq!(0b1010, set_bits_at_pos(0, 1, 0b101, 3));
        assert_eq!(0b10100, set_bits_at_pos(0, 2, 0b101, 3));
        assert_eq!(0b101000, set_bits_at_pos(0, 3, 0b101, 3));
    }

    #[test]
    fn set_bits_at_pos_with_all_ones_as_value() {
        assert_eq!(
            0b11111111111111111111111111111101,
            set_bits_at_pos(!0u32, 0, 0b101, 3)
        );
        assert_eq!(
            0b11111111111111111111111111111011,
            set_bits_at_pos(!0u32, 1, 0b101, 3)
        );
        assert_eq!(
            0b11111111111111111111111111110111,
            set_bits_at_pos(!0u32, 2, 0b101, 3)
        );
        assert_eq!(
            0b11111111111111111111111111101111,
            set_bits_at_pos(!0u32, 3, 0b101, 3)
        );
    }
}
