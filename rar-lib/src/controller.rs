use crate::peripheral::{AddressBus, ClockedDevice, MemoryMappedDevice};
use std::cell::RefCell;
use std::rc::Rc;

pub struct Controller {
    bus: Rc<RefCell<AddressBus>>,
    clocked_devices: Vec<Rc<RefCell<dyn ClockedDevice>>>,
}

impl Controller {
    pub fn new(
        memory_devices: Vec<Rc<RefCell<dyn MemoryMappedDevice>>>,
        clocked_devices: Vec<Rc<RefCell<dyn ClockedDevice>>>,
    ) -> Controller {
        Controller {
            bus: Rc::new(RefCell::new(AddressBus::new(memory_devices))),
            clocked_devices,
        }
    }

    pub fn do_tick(&mut self) {
        for dev_ref in self.clocked_devices.iter_mut() {
            dev_ref.borrow_mut().tick(self.bus.clone());
        }
    }
}
