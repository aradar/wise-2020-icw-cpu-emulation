use std::cell::RefCell;
use std::convert::TryInto;

use std::rc::Rc;

use crate::peripheral::{AddressBus, ClockedDevice};

use crate::core::decoding::{
    sign_extend, Condition, DecodedCondition, DecodedInstruction, InstructionDecoder, Operation,
    OperationStyle,
};
use crate::core::registers::CortexM0Registers;

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub enum ShiftType {
    Lsl,
    Lsr,
    Asr,
    Rrx,
    Ror,
}

// todo: add exception model: https://developer.arm.com/documentation/ddi0419/c/System-Level-Architecture/System-Level-Programmers--Model/ARMv6-M-exception-model?lang=en
// todo: add System Address Map: https://developer.arm.com/documentation/ddi0419/c/System-Level-Architecture/System-Address-Map?lang=en
// todo: system control: https://developer.arm.com/documentation/ddi0432/c/system-control
// todo: cortex-m0 exception handling https://developer.arm.com/documentation/ddi0432/c/programmers-model/exceptions/exception-handling?lang=en

#[derive(Debug)]
pub struct CortexM0Core {
    // todo: implement this based on https://developer.arm.com/documentation/dui0497/a/the-cortex-m0-processor/programmers-model/core-registers?lang=en
    // todo: Nested Vectored Interrupt Controller (NVIC)
    // todo: Debug registers
    // todo: stacks? https://developer.arm.com/documentation/dui0497/a/the-cortex-m0-processor/programmers-model/stacks?lang=en

    // todo: correctly handle reset behaviour: https://developer.arm.com/documentation/ddi0419/c/System-Level-Architecture/System-Level-Programmers--Model/ARMv6-M-exception-model/Reset-behavior?lang=en
    decoder: InstructionDecoder,
    registers: CortexM0Registers,
}

impl ClockedDevice for CortexM0Core {
    fn tick(&mut self, bus: Rc<RefCell<AddressBus>>) {
        let pc = self.registers.raw_pc();

        self.decoder
            .start_decoding(bus.borrow().read_u16(pc).unwrap());
        let decoded_inst = self.decoder.get_decoded_inst();
        println!("{:?}", decoded_inst);
        match decoded_inst {
            Some(inst) => self.execute_instruction(inst, bus),
            None => (),
        }
        self.registers.set_pc(pc + 2);
    }
}

impl CortexM0Core {
    pub fn new(initial_pc: u32, initial_sp: u32) -> CortexM0Core {
        CortexM0Core {
            decoder: InstructionDecoder::new(),
            registers: CortexM0Registers::new(initial_pc, initial_sp),
        }
    }

    fn execute_instruction(&mut self, inst: DecodedInstruction, bus: Rc<RefCell<AddressBus>>) {
        match inst.operation() {
            Operation::Adc | Operation::Add | Operation::AddSpPlus => self.execute_add(inst),
            Operation::Adr => unimplemented!(),
            Operation::And | Operation::Eor => self.execute_and_or_eor(inst),
            Operation::Asr => self.execute_asr(inst),
            Operation::B => self.execute_branch(inst),
            Operation::Bic => self.execute_bic(inst),
            Operation::Bkpt => unimplemented!(),
            Operation::Bl => unimplemented!(),
            Operation::Blx => unimplemented!(),
            Operation::Bx => unimplemented!(),
            Operation::Cmn => self.execute_cmn(inst),
            Operation::Cmp => self.execute_generic_cmp(inst),
            Operation::Cps => self.execute_cps(inst),
            Operation::Dmb => unimplemented!(),
            Operation::Dsb => unimplemented!(),
            Operation::Isb => unimplemented!(),
            Operation::Ldm => unimplemented!(),
            Operation::Ldr
            | Operation::Ldrb
            | Operation::Ldrh
            | Operation::Ldrsb
            | Operation::Ldrsh => self.execute_load_data(inst, bus),
            Operation::Lsl | Operation::Lsr => self.execute_lsl_or_lsr(inst),
            Operation::Mov => self.execute_mov(inst),
            Operation::Mrs => unimplemented!(),
            Operation::Msr => unimplemented!(),
            Operation::Mul => self.execute_mul(inst),
            Operation::Mvn => self.execute_mvn(inst),
            Operation::Nop => (),
            Operation::Orr => self.execute_orr(inst),
            Operation::Pop => self.execute_pop(inst, bus),
            Operation::Push => self.execute_push(inst, bus),
            Operation::Rev | Operation::Rev16 | Operation::Revsh => self.execute_generic_rev(inst),
            Operation::Ror => self.execute_ror(inst),
            Operation::Rsb => self.execute_rsb(inst),
            Operation::Sev => unimplemented!(),
            Operation::Stm => unimplemented!(),
            Operation::Str | Operation::Strb | Operation::Strh => {
                self.execute_store_data(inst, bus)
            }
            Operation::Sub | Operation::SubSpMinus | Operation::Sbc => self.execute_sub(inst),
            Operation::Svc => self.execute_svc(inst),
            Operation::Sxtb | Operation::Sxth => self.execute_sxtb_or_sxth(inst),
            Operation::Tst => self.execute_tst(inst),
            Operation::Udf => unimplemented!(),
            Operation::Uxtb | Operation::Uxth => self.execute_uxtb_or_uxth(inst),
            Operation::Wfe => self.execute_wfe(inst),
            Operation::Wfi => self.execute_wfi(inst),
            Operation::Yield => self.execute_yield(inst),
            Operation::NotImplemented => unimplemented!(),
        }
    }

    fn execute_pop(&mut self, instruction: DecodedInstruction, bus: Rc<RefCell<AddressBus>>) {
        let reg_list = instruction.regs_list().unwrap();
        let mut address = self.registers.sp();

        for i in 0..8 {
            let mask = 1usize << i;
            if (reg_list as usize & mask) != 0 {
                self.registers[i] = bus.borrow().read(address).unwrap();
                address += 4;
            }
        }

        if ((reg_list as usize) & (1usize << 15)) == 1 {
            self.bx_write_pc(bus.borrow().read(address).unwrap());
        }

        *self.registers.sp_mut() = self.registers.sp() + 4 * reg_list.count_zeros();
    }

    fn execute_push(&mut self, instruction: DecodedInstruction, bus: Rc<RefCell<AddressBus>>) {
        let reg_list = instruction.regs_list().unwrap();
        let offset = 4 * reg_list.count_zeros();
        let mut address = self.registers.sp() - offset;

        for i in 0..15 {
            let mask = 1usize << i;
            if (reg_list as usize & mask) != 0 {
                // todo: does this need a check to be aligned?
                (*bus)
                    .borrow_mut()
                    .write(address, self.registers[i])
                    .unwrap();
                address += 4;
            }
        }

        *self.registers.sp_mut() = self.registers.sp() - offset;
    }

    fn execute_store_data(
        &mut self,
        instruction: DecodedInstruction,
        bus: Rc<RefCell<AddressBus>>,
    ) {
        // todo: check if all cases are implemented

        let source_reg = instruction.dest_reg().unwrap();
        let offset = self.sop2_val_or_imm(instruction, 0);
        let base = self.op1_val(instruction);

        // this currently only supports offset adding as the documentation
        // always uses true for the add value flag and therefore a
        // subtraction is not reachable.
        let address = base + offset;
        let value = self.registers[source_reg];

        match instruction.operation() {
            Operation::Strb => (*bus)
                .borrow_mut()
                .write_u8(address, value.to_ne_bytes()[0] as u8)
                .unwrap(),
            Operation::Str => (*bus).borrow_mut().write(address, value).unwrap(),
            Operation::Strh => {
                let full_bytes = value.to_ne_bytes();
                let (lh_bytes, _) = full_bytes.split_at(std::mem::size_of::<u16>());
                (*bus)
                    .borrow_mut()
                    .write_u16(address, u16::from_ne_bytes(lh_bytes.try_into().unwrap()))
                    .unwrap();
            }
            _ => unreachable!(),
        };
    }

    fn execute_load_data(&mut self, instruction: DecodedInstruction, bus: Rc<RefCell<AddressBus>>) {
        // todo: check if all cases are implemented

        let dest_reg = instruction.dest_reg().unwrap();
        let offset = self.sop2_val_or_imm(instruction, 0);
        let base = if instruction.operation() == Operation::Ldr
            && instruction.style() == OperationStyle::Literal
        {
            self.align(self.registers.pc(), 4) // todo: this must be + 4
        } else {
            self.op1_val(instruction)
        };

        // this currently only supports offset adding as the documentation
        // always uses true for the add value flag and therefore a
        // subtraction is not reachable.
        let address = base + offset;

        self.registers[dest_reg] = match instruction.operation() {
            Operation::Ldrb => bus.borrow().read_u8(address).unwrap() as u32,
            Operation::Ldrh => bus.borrow().read_u16(address).unwrap() as u32,
            Operation::Ldr => bus.borrow().read(address).unwrap(),
            Operation::Ldrsb => sign_extend(bus.borrow().read_u8(address).unwrap() as u32, 8),
            Operation::Ldrsh => sign_extend(bus.borrow().read_u16(address).unwrap() as u32, 16),
            _ => unreachable!(),
        };

        // writeback is not supported as it is also always false in the
        // documentation.
    }

    fn execute_mov(&mut self, instruction: DecodedInstruction) {
        if !self.condition_passed(instruction.condition()) {
            return;
        }

        let dest_reg = instruction.dest_reg().unwrap();
        let result = self.op1_val_or_imm(instruction);

        if dest_reg == CortexM0Registers::PC_IDX {
            self.branch_write_pc(result);
        } else {
            self.registers[dest_reg] = result;

            if instruction.setflags().unwrap_or(!self.in_it_block()) {
                // the documentation sets NZC in case of a MOV with immediate but uses the current
                // carry bit as carry value. As this is the same as not setting it with the value
                // it is skipped here.
                self.registers.set_NZ_flags(result);
            }
        }
    }

    fn execute_lsl_or_lsr(&mut self, instruction: DecodedInstruction) {
        if !self.condition_passed(instruction.condition()) {
            return;
        }

        let val = self.op1_val(instruction);
        let amount = self.op1_val_or_imm(instruction).to_ne_bytes()[0] as u32;

        let (result, carry) = self.shift_with_carry(
            val,
            match instruction.operation() {
                Operation::Lsl => ShiftType::Lsl,
                Operation::Lsr => ShiftType::Lsr,
                _ => unreachable!(),
            },
            amount,
            self.registers.psr.carry_or_borrow_bit(),
        );

        self.registers[instruction.dest_reg().unwrap()] = result;

        if !self.in_it_block() {
            self.registers.set_NZC_flags(result, carry);
        }
    }

    fn execute_generic_cmp(&mut self, instruction: DecodedInstruction) {
        if !self.condition_passed(instruction.condition()) {
            return;
        }

        let first_val = self.op1_val(instruction);
        let second_val = self.sop2_val_or_imm(instruction, 0);

        let (result, carry, overflow) = self.add_with_carry(first_val, !second_val, true);

        self.registers.set_NZCV_flags(result, carry, overflow);
    }

    fn execute_cmn(&mut self, instruction: DecodedInstruction) {
        if !self.condition_passed(instruction.condition()) {
            return;
        }

        let first_val = self.op1_val(instruction);
        let shifted = self.sop2_val_or_imm(instruction, 0);

        let (result, carry, overflow) = self.add_with_carry(first_val, shifted, false);

        if !self.in_it_block() {
            self.registers.set_NZCV_flags(result, carry, overflow);
        }
    }

    fn execute_cps(&mut self, instruction: DecodedInstruction) {
        if !self.registers.control_priv_bit() {
            return;
        }

        self.registers.primask = instruction.immediate().unwrap();
    }

    fn execute_and_or_eor(&mut self, instruction: DecodedInstruction) {
        if !self.condition_passed(instruction.condition()) {
            return;
        }

        let dest_reg = instruction.dest_reg().unwrap();
        let first_val = self.op1_val(instruction);
        let second_val = self.op2_val(instruction);

        let (shifted, carry) = self.shift_with_carry(
            second_val,
            ShiftType::Lsl,
            0,
            self.registers.psr.carry_or_borrow_bit(),
        );

        let result = if instruction.operation() == Operation::And {
            first_val & shifted
        } else {
            first_val ^ shifted // todo: is the bitwise xor the right one?
        };
        self.registers[dest_reg] = result;

        if !self.in_it_block() {
            self.registers.set_NZC_flags(result, carry);
        }
    }

    fn execute_mul(&mut self, instruction: DecodedInstruction) {
        if !self.condition_passed(instruction.condition()) {
            return;
        }

        let dest_reg = instruction.dest_reg().unwrap();
        let first_val = self.op1_val(instruction);
        let second_val = self.op2_val(instruction);

        // do the calculation with 64 bit types and convert back to 32 as this results in the
        // correct overflow behaviour as only the first 4 bytes are preserved
        let result = (first_val as u64 * second_val as u64) as u32;
        self.registers[dest_reg] = result;

        if !self.in_it_block() {
            self.registers.set_NZ_flags(result);
        }
    }

    fn execute_mvn(&mut self, instruction: DecodedInstruction) {
        if !self.condition_passed(instruction.condition()) {
            return;
        }

        let dest_reg = instruction.dest_reg().unwrap();
        let val = self.op1_val(instruction);

        let (shifted_val, carry) = self.shift_with_carry(
            val,
            ShiftType::Lsl,
            0,
            self.registers.psr.carry_or_borrow_bit(),
        );

        let result = !shifted_val;
        self.registers[dest_reg] = result;

        if !self.in_it_block() {
            self.registers.set_NZC_flags(result, carry);
        }

        // todo: add tests for this
    }

    fn execute_orr(&mut self, instruction: DecodedInstruction) {
        if !self.condition_passed(instruction.condition()) {
            return;
        }

        let dest_reg = instruction.dest_reg().unwrap();
        let first_val = self.op1_val(instruction);
        let second_val = self.op2_val(instruction);

        let (shifted_sec_val, carry) = self.shift_with_carry(
            second_val,
            ShiftType::Lsl,
            0,
            self.registers.psr.carry_or_borrow_bit(),
        );

        let result = first_val | shifted_sec_val;
        self.registers[dest_reg] = result;

        if !self.in_it_block() {
            self.registers.set_NZC_flags(result, carry);
        }

        // todo: add tests for this
    }

    fn execute_generic_rev(&mut self, instruction: DecodedInstruction) {
        if !self.condition_passed(instruction.condition()) {
            return;
        }

        let dest_reg = instruction.dest_reg().unwrap();
        let value = self.op1_val(instruction);

        let result = match instruction.operation() {
            // Byte-Reverse Word reverses the byte order in a 32-bit register.
            Operation::Rev => value.swap_bytes(),

            // Byte-Reverse Packed Halfword reverses the byte order in each 16-bit halfword of a
            // 32-bit register.
            Operation::Rev16 => {
                let bytes = value.to_ne_bytes();
                u32::from_le_bytes([bytes[1], bytes[0], bytes[3], bytes[2]])
            }

            // Byte-Reverse Signed Halfword reverses the byte order in the lower 16-bit halfword of
            // a 32-bit register, and sign extends the result to 32 bits.
            Operation::Revsh => {
                let bytes = value.to_ne_bytes();
                let upper_bytes = sign_extend(bytes[0] as u32, 24).to_ne_bytes();
                u32::from_le_bytes([bytes[1], upper_bytes[0], upper_bytes[1], upper_bytes[2]])
            }

            _ => {
                panic!(
                    "{} is not a Rev instruction!",
                    instruction.operation().to_string()
                )
            }
        };

        self.registers[dest_reg] = result;

        // todo: test me
    }

    fn execute_ror(&mut self, instruction: DecodedInstruction) {
        if !self.condition_passed(instruction.condition()) {
            return;
        }

        let dest_reg = instruction.dest_reg().unwrap();
        let first_val = self.op1_val(instruction);
        let second_val = self.op2_val(instruction);

        let (result, carry) = self.shift_with_carry(
            first_val,
            ShiftType::Ror,
            second_val.to_le_bytes()[0] as u32,
            self.registers.psr.carry_or_borrow_bit(),
        );

        self.registers[dest_reg] = result;

        if !self.in_it_block() {
            self.registers.set_NZC_flags(result, carry);
        }

        // todo: add tests for this
    }

    fn execute_rsb(&mut self, instruction: DecodedInstruction) {
        if !self.condition_passed(instruction.condition()) {
            return;
        }

        let dest_reg = instruction.dest_reg().unwrap();
        let value = self.op1_val(instruction);
        let immediate = instruction.immediate().unwrap_or(0);

        let (result, carry, overflow) = self.add_with_carry(!value, immediate, true);

        self.registers[dest_reg] = result;

        if !self.in_it_block() {
            self.registers.set_NZCV_flags(result, carry, overflow);
        }

        // todo: test me
    }

    fn execute_svc(&mut self, _instruction: DecodedInstruction) {
        // https://developer.arm.com/documentation/ddi0419/c/Application-Level-Architecture/Thumb-Instruction-Details/Alphabetical-list-of-ARMv6-M-Thumb-instructions/SEV?lang=en

        // No additional decoding required

        // if ConditionPassed() then
        //     EncodingSpecificOperations();
        //     Hint_SendEvent();

        todo!()
    }

    fn execute_sxtb_or_sxth(&mut self, instruction: DecodedInstruction) {
        if !self.condition_passed(instruction.condition()) {
            return;
        }

        let dest_reg = instruction.dest_reg().unwrap();
        let value = self.op1_val(instruction);
        let rotation = instruction.immediate().unwrap_or(0);

        let value = self.shift(
            value,
            ShiftType::Ror,
            rotation,
            false, // todo: is always false here correct?
        );

        let value_bytes = value.to_le_bytes();
        self.registers[dest_reg] = match instruction.operation() {
            Operation::Sxtb => sign_extend(
                u16::from_le_bytes([value_bytes[0], value_bytes[1]]) as u32,
                16,
            ),
            Operation::Sxth => sign_extend(value_bytes[0] as u32, 8),
            _ => unreachable!(),
        };

        // todo: test me
    }

    fn execute_tst(&mut self, instruction: DecodedInstruction) {
        if !self.condition_passed(instruction.condition()) {
            return;
        }

        let first_val = self.op1_val(instruction);
        let second_val = self.op2_val(instruction);

        let (shifted, carry) = self.shift_with_carry(
            second_val,
            ShiftType::Lsl,
            0,
            self.registers.psr.carry_or_borrow_bit(),
        );

        let result = first_val & shifted;

        if !self.in_it_block() {
            self.registers.set_NZC_flags(result, carry);
        }

        // todo: test me
    }

    fn execute_uxtb_or_uxth(&mut self, instruction: DecodedInstruction) {
        if !self.condition_passed(instruction.condition()) {
            return;
        }

        let dest_reg = instruction.dest_reg().unwrap();
        let value = self.op1_val(instruction);
        let rotation = instruction.immediate().unwrap_or(0);

        let value = self.shift(
            value,
            ShiftType::Ror,
            rotation,
            false, // todo: is always false here correct?
        );

        let value_bytes = value.to_le_bytes();
        self.registers[dest_reg] = match instruction.operation() {
            Operation::Sxtb => u16::from_le_bytes([value_bytes[0], value_bytes[1]]) as u32,
            Operation::Sxth => value_bytes[0] as u32,
            _ => unreachable!(),
        };

        // todo: test me
        // todo: merge with the execute_sxtb_or_sxth method?
    }

    fn execute_wfe(&mut self, _instruction: DecodedInstruction) {
        // https://developer.arm.com/documentation/ddi0419/c/Application-Level-Architecture/Thumb-Instruction-Details/Alphabetical-list-of-ARMv6-M-Thumb-instructions/WFE?lang=en

        // No additional decoding required

        // if ConditionPassed() then
        //     EncodingSpecificOperations();
        //     if EventRegistered() then
        //         ClearEventRegister();
        //     else
        //         WaitForEvent();

        todo!()
    }

    fn execute_wfi(&mut self, _instruction: DecodedInstruction) {
        // https://developer.arm.com/documentation/ddi0419/c/Application-Level-Architecture/Thumb-Instruction-Details/Alphabetical-list-of-ARMv6-M-Thumb-instructions/WFI

        // No additional decoding required

        // if ConditionPassed() then
        //     EncodingSpecificOperations();
        //     WaitForInterrupt();

        todo!()
    }

    fn execute_yield(&mut self, _instruction: DecodedInstruction) {
        // https://developer.arm.com/documentation/ddi0419/c/Application-Level-Architecture/Thumb-Instruction-Details/Alphabetical-list-of-ARMv6-M-Thumb-instructions/YIELD?lang=en

        // No additional decoding required

        // if ConditionPassed() then
        //     EncodingSpecificOperations();
        //     Hint_Yield();

        todo!()
    }

    fn execute_bic(&mut self, instruction: DecodedInstruction) {
        if !self.condition_passed(instruction.condition()) {
            return;
        }

        let first_val = self.op1_val(instruction);
        let second_val = self.op2_val(instruction);

        let (shifted, carry) = self.shift_with_carry(
            second_val,
            ShiftType::Lsl,
            0,
            self.registers.psr.carry_or_borrow_bit(),
        );

        let result = first_val & !shifted;

        if !self.in_it_block() {
            self.registers.set_NZC_flags(result, carry);
        }
    }

    fn execute_branch(&mut self, instruction: DecodedInstruction) {
        if !self.condition_passed(instruction.condition()) {
            return;
        }

        if self.in_it_block() {
            return; // todo: this should be treated as a UNPREDICTABLE
        }

        // todo: raw_pc?
        self.branch_write_pc(
            (self.registers.raw_pc() as i32 + instruction.immediate().unwrap() as i32) as u32,
        );
    }

    fn execute_asr(&mut self, instruction: DecodedInstruction) {
        let dest_reg = instruction.dest_reg().unwrap();
        let op1_val = self.op1_val(instruction);
        let op2_val = self.op2_val_or_imm(instruction);
        let amount = op2_val.to_ne_bytes()[0] as u32;

        let (result, carry) = self.shift_with_carry(
            op1_val,
            ShiftType::Asr,
            amount,
            self.registers.psr.carry_or_borrow_bit(),
        );
        self.registers[dest_reg] = result;

        if !self.in_it_block() {
            self.registers.set_NZC_flags(result, carry);
        }
    }

    fn execute_sub(&mut self, instruction: DecodedInstruction) {
        if !self.condition_passed(instruction.condition()) {
            return;
        }

        // todo: add SBC{S}

        let dest_reg = instruction.dest_reg().unwrap();
        let op1_val = self.op1_val(instruction);
        let op2_val = self.sop2_val_or_imm(instruction, 0);

        let (result, carry, overflow) = self.add_with_carry(op1_val, !op2_val, true);
        self.registers[dest_reg] = result;

        if instruction.setflags().unwrap_or(!self.in_it_block()) {
            self.registers.set_NZCV_flags(result, carry, overflow);
        };
    }

    fn execute_add(&mut self, instruction: DecodedInstruction) {
        if !self.condition_passed(instruction.condition()) {
            return;
        }

        let dest_reg = instruction.dest_reg().unwrap();
        let op1_val = self.op1_val(instruction);
        let op2_val = self.sop2_val_or_imm(instruction, 0);
        let op = instruction.operation();

        let setflags = instruction.setflags().unwrap_or_else(|| {
            if op == Operation::Add
                && instruction.first_op_reg().unwrap_or(0) == CortexM0Registers::PC_IDX
            {
                return true;
            }

            !self.in_it_block()
        });

        let carry_bit = if op != Operation::Adc {
            false
        } else {
            self.registers.psr.carry_or_borrow_bit()
        };

        let (result, carry, overflow) = self.add_with_carry(op1_val, op2_val, carry_bit);

        if dest_reg == CortexM0Registers::PC_IDX
            && (op == Operation::AddSpPlus || op == Operation::Add)
        {
            self.branch_write_pc(result)
        } else {
            self.registers[dest_reg] = result;
        }

        if setflags {
            self.registers.set_NZCV_flags(result, carry, overflow);
        }
    }

    fn op1_val(&self, instruction: DecodedInstruction) -> u32 {
        self.registers[instruction.first_op_reg().unwrap()]
    }

    fn op2_val(&self, instruction: DecodedInstruction) -> u32 {
        self.registers[instruction.sec_op_reg().unwrap()]
    }

    #[allow(dead_code)]
    fn sop1_val_or_imm(&self, instruction: DecodedInstruction, shift: u32) -> u32 {
        self.reg_opt_or_imm(instruction, instruction.first_op_reg(), Some(shift))
    }

    fn op1_val_or_imm(&self, instruction: DecodedInstruction) -> u32 {
        self.reg_opt_or_imm(instruction, instruction.first_op_reg(), None)
    }

    fn sop2_val_or_imm(&self, instruction: DecodedInstruction, shift: u32) -> u32 {
        self.reg_opt_or_imm(instruction, instruction.sec_op_reg(), Some(shift))
    }

    fn op2_val_or_imm(&self, instruction: DecodedInstruction) -> u32 {
        self.reg_opt_or_imm(instruction, instruction.sec_op_reg(), None)
    }

    fn reg_opt_or_imm(
        &self,
        instruction: DecodedInstruction,
        reg_opt: Option<usize>,
        shift: Option<u32>,
    ) -> u32 {
        match instruction.immediate() {
            Some(val) => val,
            None => {
                let val = self.registers[reg_opt.unwrap()];
                match shift {
                    Some(amount) => self.shift(
                        val,
                        ShiftType::Lsl,
                        amount,
                        self.registers.psr.carry_or_borrow_bit(),
                    ),
                    None => val,
                }
            }
        }
    }

    fn align(&self, x: u32, y: u32) -> u32 {
        // todo: this does not support the bitstring variant. noted here https://developer.arm.com/documentation/ddi0419/c/Appendices/Pseudocode-Definition/Operators-and-built-in-functions/Arithmetic?lang=en#CHDCFDGD
        //  to support it the result of this function must be truncated to only the first
        //  num_bits_of(x) bits
        y * (x / y)
    }

    fn shift(&self, value: u32, shift_type: ShiftType, amount: u32, carry: bool) -> u32 {
        let (result, _) = self.shift_with_carry(value, shift_type, amount, carry);
        result
    }

    fn shift_with_carry(
        &self,
        value: u32,
        shift_type: ShiftType,
        amount: u32,
        carry: bool,
    ) -> (u32, bool) {
        assert!(!(shift_type == ShiftType::Rrx && amount != 1));

        if amount == 0 {
            return (value, carry);
        }

        match shift_type {
            ShiftType::Lsl => value.overflowing_shl(amount),
            ShiftType::Lsr => value.overflowing_shr(amount),
            ShiftType::Asr => {
                let (signed_value, carry_out) = (value as i32).overflowing_shr(amount);
                (signed_value as u32, carry_out)
            }
            ShiftType::Rrx => {
                panic!("ShiftType RRX is not supported on ARMv6-M")
            }
            ShiftType::Ror => {
                let result = value.rotate_right(amount);
                let mask = 1u32 << 31;
                (result, result & mask == mask)
            }
        }
    }

    fn bx_write_pc(&mut self, address: u32) {
        // todo: implement this

        // if CurrentMode == Mode_Handler && address<31:28> == '1111' then
        //     ExceptionReturn(address<27:0>);
        // else
        //     EPSR.T = address<0>;  // if EPSR.T == 0, a HardFault
        //     // is taken on the next instruction
        //     BranchTo(address<31:1>:'0');

        self.branch_to(address & !1u32);
    }

    fn branch_write_pc(&mut self, address: u32) {
        // writes address with last bit set to 0 to the pc
        self.branch_to(address & !1u32);
    }

    fn branch_to(&mut self, address: u32) {
        self.registers.set_pc(address);
    }

    fn in_it_block(&self) -> bool {
        // always false for ARMv6-M
        false
    }

    fn condition_passed(&self, dec_cond: Option<DecodedCondition>) -> bool {
        if dec_cond.is_none() {
            return true;
        }

        match dec_cond.unwrap().condition() {
            Condition::Eq => self.registers.psr.zero_bit(),
            Condition::Ne => !self.registers.psr.zero_bit(),
            Condition::Cs => self.registers.psr.carry_or_borrow_bit(),
            Condition::Cc => !self.registers.psr.carry_or_borrow_bit(),
            Condition::Mi => self.registers.psr.negative_bit(),
            Condition::Pl => !self.registers.psr.negative_bit(),
            Condition::Vs => self.registers.psr.overflow_bit(),
            Condition::Vc => !self.registers.psr.overflow_bit(),
            Condition::Hi => {
                self.registers.psr.carry_or_borrow_bit() && !self.registers.psr.zero_bit()
            }
            Condition::Ls => {
                !(self.registers.psr.carry_or_borrow_bit() && !self.registers.psr.zero_bit())
            }
            Condition::Ge => self.registers.psr.negative_bit() == self.registers.psr.overflow_bit(),
            Condition::Lt => {
                !(self.registers.psr.negative_bit() == self.registers.psr.overflow_bit())
            }
            Condition::Gt => {
                self.registers.psr.negative_bit() == self.registers.psr.overflow_bit()
                    && !self.registers.psr.zero_bit()
            }
            Condition::Le => {
                !(self.registers.psr.negative_bit() == self.registers.psr.overflow_bit()
                    && !self.registers.psr.zero_bit())
            }
            Condition::Al => true,
        }
    }

    fn add_with_carry(&self, x: u32, y: u32, carry: bool) -> (u32, bool, bool) {
        // todo: does this need a version which supports x and y with length
        //  N instead of fixed 32?

        let unsigned_sum = x as u64 + y as u64 + carry as u64;
        let signed_sum = x as i32 as i64 + y as i32 as i64 + carry as i64;
        let result = (unsigned_sum & u32::MAX as u64) as u32;
        let carry_out = result as u64 != unsigned_sum;
        let overflow = (result as i32) as i64 != signed_sum;
        (result, carry_out, overflow)
    }
}

#[cfg(test)]
mod tests {
    use super::CortexM0Core;

    fn build_no_mem_cpu() -> CortexM0Core {
        CortexM0Core::new(0, 0x20001000)
    }

    #[test]
    fn add_with_carry_no_overflow_no_carry_in() {
        let cpu = build_no_mem_cpu();
        let (result, carry_out, overflow) = cpu.add_with_carry(50, 50, false);
        assert_eq!(result, 100);
        assert_eq!(carry_out, false);
        assert_eq!(overflow, false);
    }

    #[test]
    fn add_with_carry_no_overflow_carry_in() {
        let cpu = build_no_mem_cpu();
        let (result, carry_out, overflow) = cpu.add_with_carry(50, 50, true);
        assert_eq!(result, 101);
        assert_eq!(carry_out, false);
        assert_eq!(overflow, false);
    }

    #[test]
    fn add_with_carry_overflow_no_carry_in() {
        let cpu = build_no_mem_cpu();
        let (result, carry_out, overflow) = cpu.add_with_carry(u32::MAX, 1, false);
        assert_eq!(result, 0);
        assert_eq!(carry_out, true);
        assert_eq!(overflow, false);
    }

    #[test]
    fn add_with_carry_overflow_carry_in() {
        let cpu = build_no_mem_cpu();
        let (result, carry_out, overflow) = cpu.add_with_carry(u32::MAX, 1, true);
        assert_eq!(result, 1);
        assert_eq!(carry_out, true);
        assert_eq!(overflow, false);
    }

    #[test]
    fn sub_with_carry_no_overflow() {
        let cpu = build_no_mem_cpu();
        let (result, carry_out, overflow) = cpu.add_with_carry(50, !25, true);
        assert_eq!(result as i32, 25);
        assert_eq!(carry_out, true);
        assert_eq!(overflow, false);
    }

    #[test]
    fn sub_with_carry_overflow() {
        let cpu = build_no_mem_cpu();
        let (result, carry_out, overflow) = cpu.add_with_carry(50, !100, true);
        assert_eq!(result as i32, -50);
        assert_eq!(carry_out, false);
        assert_eq!(overflow, false);
    }

    fn execute_mul(
        first_val: u32,
        sec_val: u32,
        arg_reg: Option<usize>,
        dest_reg: Option<usize>,
    ) -> CortexM0Core {
        let mut cpu = build_no_mem_cpu();

        let arg_reg = arg_reg.unwrap_or(1);
        let dest_reg = dest_reg.unwrap_or(0);

        cpu.registers[arg_reg] = first_val;
        cpu.registers[dest_reg] = sec_val;

        let mask = 0x7usize; // grab lowest 3 bits
        let inst = 0b010000_1101_000_000u16 | ((arg_reg & mask) << 3 | dest_reg & mask) as u16;

        cpu.decoder.start_decoding(inst);
        let inst = cpu.decoder.get_decoded_inst().unwrap();
        cpu.execute_mul(inst);

        return cpu;
    }

    #[test]
    fn mul_with_pos_nums_and_no_overflow() {
        let cpu = execute_mul(10, 10, None, None);
        assert_eq!(cpu.registers.registers[0], 10 * 10);
        assert_eq!(cpu.registers.psr.negative_bit(), false);
        assert_eq!(cpu.registers.psr.zero_bit(), false);
    }

    #[test]
    fn mul_with_pos_nums_and_a_overflow() {
        let cpu = execute_mul(u32::MAX, u32::MAX, None, None);
        assert_eq!(
            cpu.registers.registers[0],
            (u32::MAX as u64 * u32::MAX as u64) as u32
        );
        assert_eq!(cpu.registers.psr.negative_bit(), false);
        assert_eq!(cpu.registers.psr.zero_bit(), false);
    }

    #[test]
    fn mul_with_neg_nums_and_no_overflow() {
        let cpu = execute_mul(-10i32 as u32, 10, None, None);
        assert_eq!(cpu.registers.registers[0] as i32, -10 * 10);
        assert_eq!(cpu.registers.psr.negative_bit(), true);
        assert_eq!(cpu.registers.psr.zero_bit(), false);
    }

    #[test]
    fn mul_with_one_zero() {
        let cpu = execute_mul(0, 10, None, None);
        assert_eq!(cpu.registers.registers[0], 0 * 10);
        assert_eq!(cpu.registers.psr.negative_bit(), false);
        assert_eq!(cpu.registers.psr.zero_bit(), true);
    }

    #[test]
    fn mul_with_one_zero_and_negative_number() {
        let cpu = execute_mul(0, -10i32 as u32, None, None);
        assert_eq!(cpu.registers.registers[0] as i32, 0 * -10);
        assert_eq!(cpu.registers.psr.negative_bit(), false);
        assert_eq!(cpu.registers.psr.zero_bit(), true);
    }
}
