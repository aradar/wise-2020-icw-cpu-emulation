mod decoding;
mod processing;
mod registers;

pub use self::processing::CortexM0Core;
