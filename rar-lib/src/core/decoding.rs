use bitmatch::bitmatch;

pub fn sign_extend(number: u32, num_bits: usize) -> u32 {
    let num_shift = 32 - num_bits;
    ((number << num_shift) as i32 >> num_shift) as u32
}

#[derive(Copy, Clone, Debug, Eq, PartialEq, strum_macros::Display)]
pub enum Operation {
    Adc,
    Add,
    AddSpPlus,
    Adr,
    And,
    Asr,
    B,
    Bic,
    Bkpt,
    Bl,
    Blx,
    Bx,
    Cmn,
    Cmp,
    Cps,
    Dmb,
    Dsb,
    Eor,
    Isb,
    Ldm,
    Ldr,
    Ldrb,
    Ldrh,
    Ldrsb,
    Ldrsh,
    Lsl,
    Lsr,
    Mov,
    Mrs,
    Msr,
    Mul,
    Mvn,
    Nop,
    Orr,
    Pop,
    Push,
    Rev,
    Rev16,
    Revsh,
    Ror,
    Rsb,
    Sbc,
    Sev,
    Stm,
    Str,
    Strb,
    Strh,
    Sub,
    SubSpMinus,
    Svc,
    Sxtb,
    Sxth,
    Tst,
    Udf,
    Uxtb,
    Uxth,
    Wfe,
    Wfi,
    Yield,
    NotImplemented,
}

impl Default for Operation {
    fn default() -> Self {
        Operation::NotImplemented
    }
}

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub enum Condition {
    Eq,
    Ne,
    Cs,
    Cc,
    Mi,
    Pl,
    Vs,
    Vc,
    Hi,
    Ls,
    Ge,
    Lt,
    Gt,
    Le,
    Al,
}

impl Condition {
    #[bitmatch]
    fn decode(bits: u8) -> Condition {
        #[bitmatch]
        match bits {
            "0000 0000" => Condition::Eq,
            "0000 0001" => Condition::Ne,
            "0000 0010" => Condition::Cs,
            "0000 0011" => Condition::Cc,
            "0000 0100" => Condition::Mi,
            "0000 0101" => Condition::Pl,
            "0000 0110" => Condition::Vs,
            "0000 0111" => Condition::Vc,
            "0000 1000" => Condition::Hi,
            "0000 1001" => Condition::Ls,
            "0000 1010" => Condition::Ge,
            "0000 1011" => Condition::Lt,
            "0000 1100" => Condition::Gt,
            "0000 1101" => Condition::Le,
            "0000 1110" => Condition::Al,
            _ => unreachable!(),
        }
    }
}

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub struct DecodedCondition {
    bits: u8,
    condition: Condition,
}

impl DecodedCondition {
    pub fn new(cond_bits: u8) -> DecodedCondition {
        DecodedCondition {
            bits: cond_bits,
            condition: Condition::decode(cond_bits),
        }
    }

    #[allow(dead_code)]
    pub fn bits(&self) -> u8 {
        self.bits
    }

    pub fn condition(&self) -> &Condition {
        &self.condition
    }
}

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub enum OperationStyle {
    None,
    Immediate,
    Register,
    Literal,
}

impl Default for OperationStyle {
    fn default() -> Self {
        OperationStyle::None
    }
}

#[derive(Copy, Clone, Default, Debug)]
pub struct DecodedInstruction {
    operation: Operation,
    style: OperationStyle,
    dest_reg: Option<usize>,
    first_op_reg: Option<usize>,
    sec_op_reg: Option<usize>,
    regs_list: Option<u16>,
    immediate: Option<u32>,
    condition: Option<DecodedCondition>,
    setflags: Option<bool>,
}

impl DecodedInstruction {
    pub fn operation(&self) -> Operation {
        self.operation
    }

    pub fn style(&self) -> OperationStyle {
        self.style
    }

    pub fn dest_reg(&self) -> Option<usize> {
        self.dest_reg
    }

    pub fn first_op_reg(&self) -> Option<usize> {
        self.first_op_reg
    }

    pub fn sec_op_reg(&self) -> Option<usize> {
        self.sec_op_reg
    }

    pub fn immediate(&self) -> Option<u32> {
        self.immediate
    }

    pub fn condition(&self) -> Option<DecodedCondition> {
        self.condition
    }

    pub fn setflags(&self) -> Option<bool> {
        self.setflags
    }

    pub fn regs_list(&self) -> Option<u16> {
        self.regs_list
    }

    #[allow(dead_code)]
    pub fn uses_imm(&self) -> bool {
        self.immediate.is_some()
    }
}

#[bitmatch]
fn decode_32bit_thumb(inst: u32) -> DecodedInstruction {
    #[bitmatch]
    match inst {
        "111?1???????????????????????????" => DecodedInstruction {
            operation: Operation::Udf,
            ..Default::default()
        },
        "11110???????????0???????????????" => DecodedInstruction {
            operation: Operation::Udf,
            ..Default::default()
        },

        // todo: add "See Branch and miscellaneous control" from
        //  https://developer.arm.com/documentation/ddi0419/c/Application-Level-Architecture/The-Thumb-Instruction-Set-Encoding/32-bit-Thumb-instruction-encoding?lang=en
        _ => Default::default(),
    }
}

fn decode_shift_imm(operation: Operation, imm: u16) -> u32 {
    match operation {
        Operation::Asr => imm as u32,
        Operation::Lsl | Operation::Lsr => (if imm != 0 { imm } else { 32 }) as u32,
        // todo: the ARMv6-M architecture does not have a RRX instruction. if this ever gets used
        //  for something > ARMv6-M it must be handled here.
        Operation::Ror => imm as u32,
        //Operation::Rrx => {}
        _ => unreachable!(),
    }
}

#[bitmatch]
#[allow(unused)]
fn decode_16bit_thumb(inst: u16) -> DecodedInstruction {
    #[bitmatch]
    match inst {
        // ------------------------------------------------------------
        // Generate PC-relative address, see ADR
        // ------------------------------------------------------------
        "1010 0 ddd iiiiiiii" => DecodedInstruction {
            // ADR
            operation: Operation::Mov,
            style: OperationStyle::Register,
            dest_reg: Some(d as usize),
            immediate: Some((i as u32) << 2),
            ..Default::default()
        },

        // ------------------------------------------------------------
        // Shift (immediate), add, subtract, move, and compare ops
        // ------------------------------------------------------------
        "00 00000 000 mmmddd" => DecodedInstruction {
            // MOVS (register)
            operation: Operation::Mov,
            style: OperationStyle::Register,
            first_op_reg: Some(m as usize),
            dest_reg: Some(d as usize),
            setflags: Some(true),
            ..Default::default()
        },
        "00 000 iiiiimmmddd" => DecodedInstruction {
            // Logical Shift Left - LSL (immediate)
            operation: Operation::Lsl,
            style: OperationStyle::Immediate,
            immediate: Some(decode_shift_imm(Operation::Lsl, i)),
            first_op_reg: Some(m as usize),
            dest_reg: Some(d as usize),
            ..Default::default()
        },
        "00 001 iiiiimmmddd" => DecodedInstruction {
            // Logical Shift Right - LSR (immediate)
            operation: Operation::Lsr,
            style: OperationStyle::Immediate,
            immediate: Some(decode_shift_imm(Operation::Lsr, i)),
            first_op_reg: Some(m as usize),
            dest_reg: Some(d as usize),
            ..Default::default()
        },
        "00 010 iiiiimmmddd" => DecodedInstruction {
            // Arithmetic Shift Right - ASR (immediate)
            operation: Operation::Asr,
            style: OperationStyle::Immediate,
            immediate: Some(decode_shift_imm(Operation::Asr, i)),
            first_op_reg: Some(m as usize),
            dest_reg: Some(d as usize),
            ..Default::default()
        },
        "00 01100 mmmnnnddd" => DecodedInstruction {
            // Add register - ADD (register)
            operation: Operation::Add,
            style: OperationStyle::Register,
            first_op_reg: Some(n as usize),
            sec_op_reg: Some(m as usize),
            dest_reg: Some(d as usize),
            ..Default::default()
        },
        "00 01101 mmmnnnddd" => DecodedInstruction {
            // Subtract register - SUB (register)
            operation: Operation::Sub,
            style: OperationStyle::Register,
            first_op_reg: Some(n as usize),
            sec_op_reg: Some(m as usize),
            dest_reg: Some(d as usize),
            ..Default::default()
        },
        "00 01110 iiinnnddd" => DecodedInstruction {
            // Add 3-bit immediate - ADD (immediate)
            operation: Operation::Add,
            style: OperationStyle::Immediate,
            immediate: Some(i as u32),
            first_op_reg: Some(n as usize),
            dest_reg: Some(d as usize),
            ..Default::default()
        },
        "00 01111 iiinnnddd" => DecodedInstruction {
            // Subtract 3-bit immediate - SUB (immediate)
            operation: Operation::Sub,
            style: OperationStyle::Immediate,
            immediate: Some(i as u32),
            first_op_reg: Some(n as usize),
            dest_reg: Some(d as usize),
            ..Default::default()
        },
        "00 100 dddiiiiiiii" => DecodedInstruction {
            // Move - MOV (immediate)
            operation: Operation::Mov,
            style: OperationStyle::Immediate,
            dest_reg: Some(d as usize),
            immediate: Some(i as u32),
            ..Default::default()
        },
        "00 101 nnniiiiiiii" => DecodedInstruction {
            // Compare - CMP (immediate)
            operation: Operation::Cmp,
            style: OperationStyle::Immediate,
            first_op_reg: Some(n as usize),
            immediate: Some(i as u32),
            ..Default::default()
        },
        "00 110 nnniiiiiiii" => DecodedInstruction {
            // Add 8-bit immediate - ADD (immediate)
            operation: Operation::Add,
            style: OperationStyle::Immediate,
            first_op_reg: Some(n as usize),
            dest_reg: Some(n as usize),
            immediate: Some(i as u32),
            ..Default::default()
        },
        "00 111 nnniiiiiiii" => DecodedInstruction {
            // Subtract 8-bit immediate - SUB (immediate)
            operation: Operation::Sub,
            style: OperationStyle::Immediate,
            first_op_reg: Some(n as usize),
            immediate: Some(i as u32),
            setflags: Some(false),
            ..Default::default()
        },

        // ------------------------------------------------------------
        // Data processing ops
        // ------------------------------------------------------------
        "010000 0000 mmmddd" => DecodedInstruction {
            // Bitwise AND - AND (register)
            operation: Operation::And,
            style: OperationStyle::Register,
            sec_op_reg: Some(m as usize),
            dest_reg: Some(d as usize),
            ..Default::default()
        },
        "010000 0001 mmmddd" => DecodedInstruction {
            // Exclusive OR - EOR (register)
            operation: Operation::Eor,
            style: OperationStyle::Register,
            sec_op_reg: Some(m as usize),
            dest_reg: Some(d as usize),
            ..Default::default()
        },
        "010000 0010 mmmddd" => DecodedInstruction {
            // Logical Shift Left - LSL (register)
            operation: Operation::Lsl,
            style: OperationStyle::Register,
            sec_op_reg: Some(m as usize),
            dest_reg: Some(d as usize),
            ..Default::default()
        },
        "010000 0011 mmmddd" => DecodedInstruction {
            // Logical Shift Right - LSR (register)
            operation: Operation::Lsr,
            style: OperationStyle::Register,
            first_op_reg: Some(d as usize),
            sec_op_reg: Some(m as usize),
            dest_reg: Some(d as usize),
            ..Default::default()
        },
        "010000 0100 mmmddd" => DecodedInstruction {
            // Arithmetic Shift Right - ASR (register)
            operation: Operation::Asr,
            style: OperationStyle::Register,
            sec_op_reg: Some(m as usize),
            dest_reg: Some(d as usize),
            ..Default::default()
        },
        "010000 0101 mmmddd" => DecodedInstruction {
            // ADC (register)
            operation: Operation::Adc,
            style: OperationStyle::Register,
            first_op_reg: Some(d as usize),
            sec_op_reg: Some(m as usize),
            dest_reg: Some(d as usize),
            ..Default::default()
        },
        "010000 0110 mmmddd" => DecodedInstruction {
            // Subtract with Carry - SBC (register)
            operation: Operation::Sbc,
            style: OperationStyle::Register,
            sec_op_reg: Some(m as usize),
            dest_reg: Some(d as usize),
            ..Default::default()
        },
        "010000 0111 mmmddd" => DecodedInstruction {
            // Rotate Right - ROR (register)
            operation: Operation::Ror,
            style: OperationStyle::Register,
            first_op_reg: Some(d as usize),
            sec_op_reg: Some(m as usize),
            dest_reg: Some(d as usize),
            ..Default::default()
        },
        "010000 1000 mmmnnn" => DecodedInstruction {
            // Set flags on bitwise AND - TST (register)
            operation: Operation::Tst,
            style: OperationStyle::Register,
            sec_op_reg: Some(m as usize),
            first_op_reg: Some(n as usize),
            ..Default::default()
        },
        "010000 1001 nnnddd" => DecodedInstruction {
            // Reverse Subtract from 0 - RSB (immediate)
            operation: Operation::Rsb,
            style: OperationStyle::Immediate,
            first_op_reg: Some(n as usize),
            dest_reg: Some(d as usize),
            ..Default::default()
        },
        "010000 1010 mmmnnn" => DecodedInstruction {
            // Compare Registers - CMP (register)
            operation: Operation::Cmp,
            style: OperationStyle::Register,
            sec_op_reg: Some(m as usize),
            first_op_reg: Some(n as usize),
            ..Default::default()
        },
        "010000 1011 mmmnnn" => DecodedInstruction {
            // Compare Negative - CMN (register)
            operation: Operation::Cmn,
            style: OperationStyle::Register,
            sec_op_reg: Some(m as usize),
            first_op_reg: Some(n as usize),
            ..Default::default()
        },
        "010000 1100 mmmddd" => DecodedInstruction {
            // Logical OR - ORR (register)
            operation: Operation::Orr,
            style: OperationStyle::Register,
            first_op_reg: Some(d as usize),
            sec_op_reg: Some(m as usize),
            dest_reg: Some(d as usize),
            ..Default::default()
        },
        "010000 1101 nnnddd" => DecodedInstruction {
            // Multiply Two Registers - MUL
            operation: Operation::Mul,
            style: OperationStyle::Register,
            first_op_reg: Some(n as usize),
            sec_op_reg: Some(d as usize),
            dest_reg: Some(d as usize),
            ..Default::default()
        },
        "010000 1110 mmmddd" => DecodedInstruction {
            // Bit Clear - BIC (register)
            operation: Operation::Bic,
            style: OperationStyle::Register,
            sec_op_reg: Some(m as usize),
            dest_reg: Some(d as usize),
            ..Default::default()
        },
        "010000 1111 mmmddd" => DecodedInstruction {
            // Bitwise NOT - MVN (register)
            operation: Operation::Mvn,
            style: OperationStyle::Register,
            first_op_reg: Some(m as usize),
            dest_reg: Some(d as usize),
            ..Default::default()
        },

        // ------------------------------------------------------------
        // Special data instructions and branch and exchange ops
        // ------------------------------------------------------------
        "010001 00 dmmmmnnn" => DecodedInstruction {
            // Add Registers - ADD (register)
            operation: Operation::Add,
            style: OperationStyle::Register,
            dest_reg: Some(d as usize),
            sec_op_reg: Some(m as usize),
            first_op_reg: Some(n as usize),
            ..Default::default()
        },
        "010001 0100 ??????" => DecodedInstruction {
            // unpredictable -
            operation: Operation::NotImplemented, // todo: maybe a NOP?
            ..Default::default()
        },
        "010001 01 nmmmmnnn" => DecodedInstruction {
            // Compare Registers - CMP (register)
            operation: Operation::Cmp,
            style: OperationStyle::Register,
            first_op_reg: Some(n as usize),
            sec_op_reg: Some(m as usize),
            ..Default::default()
        },
        "010001 10 dmmmmddd" => DecodedInstruction {
            // Move Registers - MOV (register)
            operation: Operation::Mov,
            style: OperationStyle::Register,
            first_op_reg: Some(m as usize),
            dest_reg: Some(d as usize),
            setflags: Some(false),
            ..Default::default()
        },
        "010001 110 mmmm???" => DecodedInstruction {
            // Branch and Exchange - BX
            // the last three bits which should all be zeros are ignored during
            // parsing as according to the documentation those may be set for
            // some devices
            operation: Operation::Bx,
            first_op_reg: Some(m as usize),
            ..Default::default()
        },
        "010001 111 mmmm???" => DecodedInstruction {
            // Branch with Link and Exchange - BLX (register)
            // the last three bits which should all be zeros are ignored during
            // parsing as according to the documentation those may be set for
            // some devices
            operation: Operation::Blx,
            first_op_reg: Some(m as usize),
            ..Default::default()
        },

        // ------------------------------------------------------------
        // Load from Literal Pool, see LDR (literal) ops
        // ------------------------------------------------------------
        "01001 tttiiiiiiii" => DecodedInstruction {
            // LDR (literal)
            operation: Operation::Ldr,
            style: OperationStyle::Literal,
            dest_reg: Some(t as usize),
            immediate: Some((i as u32) << 2),
            ..Default::default()
        },

        // ------------------------------------------------------------
        // Load/store single data item ops
        // ------------------------------------------------------------
        "0101 000 mmmnnnttt" => DecodedInstruction {
            // Store Register - STR (register)
            operation: Operation::Str,
            style: OperationStyle::Register,
            sec_op_reg: Some(m as usize),
            first_op_reg: Some(n as usize),
            dest_reg: Some(t as usize),
            ..Default::default()
        },
        "0101 001 mmmnnnttt" => DecodedInstruction {
            // Store Register Halfword - STRH (register)
            operation: Operation::Strh,
            style: OperationStyle::Register,
            sec_op_reg: Some(m as usize),
            first_op_reg: Some(n as usize),
            dest_reg: Some(t as usize),
            ..Default::default()
        },
        "0101 010 mmmnnnttt" => DecodedInstruction {
            // Store Register Byte - STRB (register)
            operation: Operation::Strb,
            style: OperationStyle::Register,
            sec_op_reg: Some(m as usize),
            first_op_reg: Some(n as usize),
            dest_reg: Some(t as usize),
            ..Default::default()
        },
        "0101 011 mmmnnnttt" => DecodedInstruction {
            // Load Register Signed Byte - LDRSB (register)
            operation: Operation::Ldrsb,
            style: OperationStyle::Register,
            sec_op_reg: Some(m as usize),
            first_op_reg: Some(n as usize),
            dest_reg: Some(t as usize),
            ..Default::default()
        },
        "0101 100 mmmnnnttt" => DecodedInstruction {
            // Load Register - LDR (register)
            operation: Operation::Ldr,
            style: OperationStyle::Register,
            sec_op_reg: Some(m as usize),
            first_op_reg: Some(n as usize),
            dest_reg: Some(t as usize),
            ..Default::default()
        },
        "0101 101 mmmnnnttt" => DecodedInstruction {
            // Load Register Halfword - LDRH (register)
            operation: Operation::Ldrh,
            style: OperationStyle::Register,
            sec_op_reg: Some(m as usize),
            first_op_reg: Some(n as usize),
            dest_reg: Some(t as usize),
            ..Default::default()
        },
        "0101 110 mmmnnnttt" => DecodedInstruction {
            // Load Register Byte - LDRB (register)
            operation: Operation::Ldrb,
            style: OperationStyle::Register,
            sec_op_reg: Some(m as usize),
            first_op_reg: Some(n as usize),
            dest_reg: Some(t as usize),
            ..Default::default()
        },
        "0101 111 mmmnnnttt" => DecodedInstruction {
            // Load Register Signed Halfword - LDRSH (register)
            operation: Operation::Ldrsh,
            style: OperationStyle::Register,
            sec_op_reg: Some(m as usize),
            first_op_reg: Some(n as usize),
            dest_reg: Some(t as usize),
            ..Default::default()
        },
        "0110 0 iiiiinnnttt" => DecodedInstruction {
            // Store Register - STR (immediate)
            operation: Operation::Str,
            style: OperationStyle::Immediate,
            immediate: Some((i as u32) << 2),
            first_op_reg: Some(n as usize),
            dest_reg: Some(t as usize),
            ..Default::default()
        },
        "0110 1 iiiiinnnttt" => DecodedInstruction {
            // Load Register - LDR (immediate)
            operation: Operation::Ldr,
            style: OperationStyle::Immediate,
            immediate: Some((i as u32) << 2),
            first_op_reg: Some(n as usize),
            dest_reg: Some(t as usize),
            ..Default::default()
        },
        "0111 0 iiiiinnnttt" => DecodedInstruction {
            // Store Register Byte - STRB (immediate)
            operation: Operation::Strb,
            style: OperationStyle::Immediate,
            immediate: Some(i as u32),
            first_op_reg: Some(n as usize),
            dest_reg: Some(t as usize),
            ..Default::default()
        },
        "0111 1 iiiiinnnttt" => DecodedInstruction {
            // Load Register Byte - LDRB (immediate)
            operation: Operation::Ldrb,
            style: OperationStyle::Immediate,
            immediate: Some(i as u32),
            first_op_reg: Some(n as usize),
            dest_reg: Some(t as usize),
            ..Default::default()
        },
        "1000 0 iiiiinnnttt" => DecodedInstruction {
            // Store Register Halfword - STRH (immediate)
            operation: Operation::Strh,
            style: OperationStyle::Immediate,
            immediate: Some(i as u32),
            first_op_reg: Some(n as usize),
            dest_reg: Some(t as usize),
            ..Default::default()
        },
        "1000 1 iiiiinnnttt" => DecodedInstruction {
            // Load Register Halfword - LDRH (immediate)
            operation: Operation::Ldrh,
            style: OperationStyle::Immediate,
            immediate: Some((i as u32) << 1),
            first_op_reg: Some(n as usize),
            dest_reg: Some(t as usize),
            ..Default::default()
        },
        "1001 0 iiiiinnnttt" => DecodedInstruction {
            // Store Register SP relative - STR (immediate)
            operation: Operation::Str,
            style: OperationStyle::Immediate,
            immediate: Some(i as u32),
            first_op_reg: Some(n as usize),
            dest_reg: Some(t as usize),
            ..Default::default()
        },
        "1001 1 iiiiinnnttt" => DecodedInstruction {
            // Load Register SP relative - LDR (immediate)
            operation: Operation::Ldr,
            style: OperationStyle::Immediate,
            immediate: Some((i as u32) << 2),
            first_op_reg: Some(n as usize),
            dest_reg: Some(t as usize),
            ..Default::default()
        },

        // ------------------------------------------------------------
        // Generate SP-relative address ops
        // ------------------------------------------------------------
        "10101 dddiiiiiiii" => DecodedInstruction {
            // Add Immediate to SP - ADD (SP plus immediate) T1
            operation: Operation::AddSpPlus,
            immediate: Some((i as u32) << 2),
            // the following line is a deviation of the default
            // behaviour but makes executing it more generic
            first_op_reg: Some(13),
            dest_reg: Some(d as usize),
            setflags: Some(false),
            ..Default::default()
        },

        // ------------------------------------------------------------
        // Miscellaneous 16-bit instructions
        // ------------------------------------------------------------
        "1011 00000 iiiiiii" => DecodedInstruction {
            // Add Immediate to SP - ADD (SP plus immediate) T2
            operation: Operation::AddSpPlus,
            immediate: Some((i as u32) << 2),
            // the following two are a deviation of the default
            // behaviour but make executing it more generic
            first_op_reg: Some(13),
            dest_reg: Some(13),
            setflags: Some(false),
            ..Default::default()
        },
        "1011 00001 iiiiiii" => DecodedInstruction {
            // Subtract Immediate from SP - SUB (SP minus immediate)
            operation: Operation::SubSpMinus,
            immediate: Some((i as u32) << 2),
            // the following two are a deviation of the default
            // behaviour but make executing it more generic
            first_op_reg: Some(13),
            dest_reg: Some(13),
            setflags: Some(false),
            ..Default::default()
        },
        "1011 001000 mmmddd" => DecodedInstruction {
            // Signed Extend Halfword - SXTH
            operation: Operation::Sxth,
            first_op_reg: Some(m as usize),
            dest_reg: Some(d as usize),
            ..Default::default()
        },
        "1011 001001 mmmddd" => DecodedInstruction {
            // Signed Extend Byte - SXTB
            operation: Operation::Sxtb,
            first_op_reg: Some(m as usize),
            dest_reg: Some(d as usize),
            ..Default::default()
        },
        "1011 001010 mmmddd" => DecodedInstruction {
            // Unsigned Extend Halfword - UXTH
            operation: Operation::Uxth,
            sec_op_reg: Some(m as usize),
            dest_reg: Some(d as usize),
            ..Default::default()
        },
        "1011 001011 mmmddd" => DecodedInstruction {
            // Unsigned Extend Byte - UXTB
            operation: Operation::Uxtb,
            sec_op_reg: Some(m as usize),
            dest_reg: Some(d as usize),
            ..Default::default()
        },
        "1011 010 mrrrrrrrr" => DecodedInstruction {
            // Push Multiple Registers - PUSH
            operation: Operation::Push,
            regs_list: Some((m << (6 + 8)) | r),
            ..Default::default()
        },
        "1011 0110011 i????" => DecodedInstruction {
            // Change Processor State - CPS
            operation: Operation::Cps,
            immediate: Some(i as u32),
            ..Default::default()
        },
        "1011 101000 mmmddd" => DecodedInstruction {
            // Byte-Reverse Word - REV
            operation: Operation::Rev,
            sec_op_reg: Some(m as usize),
            dest_reg: Some(d as usize),
            ..Default::default()
        },
        "1011 101001 mmmddd" => DecodedInstruction {
            // Byte-Reverse Packed Halfword - REV16
            operation: Operation::Rev16,
            sec_op_reg: Some(m as usize),
            dest_reg: Some(d as usize),
            ..Default::default()
        },
        "1011 101011 mmmddd" => DecodedInstruction {
            // Byte-Reverse Signed Halfword - REVSH
            operation: Operation::Revsh,
            sec_op_reg: Some(m as usize),
            dest_reg: Some(d as usize),
            ..Default::default()
        },
        "1011 110 prrrrrrrr" => DecodedInstruction {
            // Pop Multiple Registers - POP
            operation: Operation::Pop,
            regs_list: Some((p << (7 + 8)) | r),
            ..Default::default()
        },
        "1011 1110 iiiiiiii" => DecodedInstruction {
            // Breakpoint - BKPT
            operation: Operation::Bkpt,
            immediate: Some(i as u32),
            ..Default::default()
        },
        // todo: the last 4 bits must be > 0?
        //  https://developer.arm.com/documentation/ddi0419/c/Application-Level-Architecture/The-Thumb-Instruction-Set-Encoding/16-bit-Thumb-instruction-encoding/Miscellaneous-16-bit-instructions?lang=en#BGBFIDEI
        "1011 1111 ????bbbb" => DecodedInstruction {
            // undefined
            operation: Operation::Udf, // todo: what op?
            ..Default::default()
        },
        "1011 1111 00000000" => DecodedInstruction {
            // No Operation hint - NOP
            operation: Operation::Nop,
            ..Default::default()
        },
        "1011 1111 00010000" => DecodedInstruction {
            // Yield hint - YIELD
            operation: Operation::Yield,
            ..Default::default()
        },
        "1011 1111 00100000" => DecodedInstruction {
            // Wait for Event hint - WFE
            operation: Operation::Wfe,
            ..Default::default()
        },
        "1011 1111 00110000" => DecodedInstruction {
            // Wait for Interrupt hint - WFI
            operation: Operation::Wfi,
            ..Default::default()
        },
        "1011 1111 01000000" => DecodedInstruction {
            // Send Event hint - SEV
            operation: Operation::Sev,
            ..Default::default()
        },

        // ------------------------------------------------------------
        // Store multiple registers ops
        // ------------------------------------------------------------
        "11000 nnnllllllll" => DecodedInstruction {
            operation: Operation::Stm,
            first_op_reg: Some(n as usize),
            regs_list: Some(l),
            ..Default::default()
        },

        // ------------------------------------------------------------
        // Load multiple registers ops
        // ------------------------------------------------------------
        "11001 nnnllllllll" => DecodedInstruction {
            operation: Operation::Ldm,
            first_op_reg: Some(n as usize),
            regs_list: Some(l),
            ..Default::default()
        },

        // ------------------------------------------------------------
        // Conditional branch, and Supervisor Call ops
        // ------------------------------------------------------------
        "1101 1110 iiiiiiii" => DecodedInstruction {
            // Permanently undefined - UDF
            operation: Operation::Udf,
            immediate: Some(i as u32),
            ..Default::default()
        },
        "1101 1111 iiiiiiii" => DecodedInstruction {
            // Supervisor Call - SVC
            operation: Operation::Svc,
            immediate: Some(i as u32),
            ..Default::default()
        },
        "1101 cccc iiiiiiii" => DecodedInstruction {
            // Conditional branch - B
            operation: Operation::B,
            immediate: Some(sign_extend((i << 1) as u32, 9)),
            condition: Some(DecodedCondition::new(c as u8)),
            ..Default::default()
        },

        // ------------------------------------------------------------
        // Unconditional branch ops
        // ------------------------------------------------------------
        "11100 iiiiiiiiiii" => DecodedInstruction {
            // Unconditional branch - B
            operation: Operation::B,
            immediate: Some(sign_extend((i << 1) as u32, 12)),
            ..Default::default()
        },

        _ => Default::default(),
    }
}

#[derive(Debug)]
pub struct InstructionDecoder {
    hw_inst: u16,
    decoded_inst: Option<DecodedInstruction>,
}

impl InstructionDecoder {
    pub fn new() -> InstructionDecoder {
        InstructionDecoder {
            hw_inst: 0,
            decoded_inst: None,
        }
    }

    pub fn start_decoding(&mut self, inst: u16) {
        if InstructionDecoder::is_start_of_double_word_inst(inst) {
            self.hw_inst = inst;
            self.decoded_inst = None;
        } else if self.hw_inst != 0 {
            let fw_inst = ((self.hw_inst as u32) << 16) + inst as u32;
            self.decoded_inst = Some(decode_32bit_thumb(fw_inst));
        } else {
            self.decoded_inst = Some(decode_16bit_thumb(inst));
        }
    }

    pub fn get_decoded_inst(&self) -> Option<DecodedInstruction> {
        self.decoded_inst
    }

    fn is_start_of_double_word_inst(inst: u16) -> bool {
        let indicator = inst >> 11;
        return indicator == 0b11101 || indicator == 0b11110 || indicator == 0b11111;
    }
}

#[cfg(test)]
mod tests {
    use super::{decode_16bit_thumb, Operation, OperationStyle};

    #[test]
    fn decode_16bit_thumb_adr_mov_variant_d6_i6() {
        let instruction = decode_16bit_thumb(0b1010_0_110_00000011u16);
        assert_eq!(instruction.operation, Operation::Mov);
        assert_eq!(instruction.style, OperationStyle::Register);
        assert_eq!(instruction.dest_reg.unwrap(), 6);
        assert_eq!(instruction.immediate.unwrap(), 12);
    }

    #[test]
    fn decode_16bit_thumb_mov_d6_m6() {
        let instruction = decode_16bit_thumb(0b00_00000_000_110_110u16);
        assert_eq!(instruction.operation, Operation::Mov);
        assert_eq!(instruction.style, OperationStyle::Register);
        assert_eq!(instruction.dest_reg.unwrap(), 6);
        assert_eq!(instruction.first_op_reg.unwrap(), 6);
    }

    // todo: write more
}
