use std::ops::{Index, IndexMut};

use crate::utils::{get_bit_at_pos, set_bit_at_pos};

#[derive(Debug)]
pub struct ProgramStatusRegister {
    apsr: u32,
    ipsr: u32,
    epsr: u32,
}

#[allow(dead_code)]
impl ProgramStatusRegister {
    pub fn negative_bit(&self) -> bool {
        get_bit_at_pos(self.apsr, 31)
    }

    pub fn set_negative_bit(&mut self, bit_value: bool) {
        self.apsr = set_bit_at_pos(self.apsr, 31, bit_value);
    }

    pub fn zero_bit(&self) -> bool {
        get_bit_at_pos(self.apsr, 30)
    }

    pub fn set_zero_bit(&mut self, bit_value: bool) {
        self.apsr = set_bit_at_pos(self.apsr, 30, bit_value);
    }

    pub fn carry_or_borrow_bit(&self) -> bool {
        get_bit_at_pos(self.apsr, 29)
    }

    pub fn set_carry_or_borrow_bit(&mut self, bit_value: bool) {
        self.apsr = set_bit_at_pos(self.apsr, 29, bit_value);
    }

    pub fn overflow_bit(&self) -> bool {
        get_bit_at_pos(self.apsr, 28)
    }

    pub fn set_overflow_bit(&mut self, bit_value: bool) {
        self.apsr = set_bit_at_pos(self.apsr, 28, bit_value);
    }

    pub fn exception_num(&self) -> u32 {
        let mask = 0b111111u32;
        self.ipsr & mask
    }

    pub fn set_exception_num(&mut self, exc_num: u32) {
        let mask = 0b111111u32;
        self.ipsr &= !mask; // removal of all previous exc_num bits
        self.ipsr = exc_num & mask
    }

    pub fn thumb_state_bit(&self) -> bool {
        get_bit_at_pos(self.epsr, 24)
    }

    pub fn set_thump_state_bit(&mut self, bit_value: bool) {
        self.epsr = set_bit_at_pos(self.epsr, 24, bit_value);
    }
}

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub enum ExecutionMode {
    Thread,
    Handler,
}

#[derive(Debug)]
pub struct CortexM0Registers {
    current_mode: ExecutionMode,
    pub registers: [u32; 12], // todo: this is currently public for the unit tests
    psp: u32,
    msp: u32,
    lr: u32,
    raw_pc: u32,
    readable_pc: u32,
    pub psr: ProgramStatusRegister,
    pub primask: u32,
    control: u32,
}

impl Index<usize> for CortexM0Registers {
    type Output = u32;

    fn index(&self, index: usize) -> &Self::Output {
        assert!(index <= 15);

        match index {
            0..=12 => &self.registers[index],
            13 => {
                if self.control_spsel_bit() {
                    &self.psp
                } else {
                    &self.msp
                }
            }
            14 => &self.lr,
            15 => &self.readable_pc,
            _ => unreachable!(),
        }
    }
}

impl IndexMut<usize> for CortexM0Registers {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        assert!(index <= 14);

        match index {
            0..=12 => &mut self.registers[index],
            13 => {
                if self.control_spsel_bit() {
                    &mut self.psp
                } else {
                    &mut self.msp
                }
            }
            14 => &mut self.lr,
            // no pc as noted in the docs
            _ => unreachable!(),
        }
    }
}

#[allow(dead_code)]
impl CortexM0Registers {
    pub const SP_IDX: usize = 13;
    pub const LR_IDX: usize = 14;
    pub const PC_IDX: usize = 15;

    pub fn new(pc: u32, sp: u32) -> CortexM0Registers {
        CortexM0Registers {
            current_mode: ExecutionMode::Thread,
            registers: [0; 12],
            psp: 0,
            msp: sp,
            lr: 0,
            raw_pc: pc,
            readable_pc: pc + 4,
            psr: ProgramStatusRegister {
                apsr: 0,
                ipsr: 0,
                epsr: 0,
            },
            primask: 0,
            control: 0,
        }
    }

    #[allow(non_snake_case)]
    pub fn set_NZ_flags(&mut self, result: u32) {
        self.set_flags(result, true, true, None, None);
    }

    #[allow(non_snake_case)]
    pub fn set_NZC_flags(&mut self, result: u32, carry: bool) {
        self.set_flags(result, true, true, Some(carry), None);
    }

    #[allow(non_snake_case)]
    pub fn set_NZCV_flags(&mut self, result: u32, carry: bool, overflow: bool) {
        self.set_flags(result, true, true, Some(carry), Some(overflow));
    }

    pub fn set_flags(
        &mut self,
        result: u32,
        set_negative: bool,
        set_zero: bool,
        carry: Option<bool>,
        overflow: Option<bool>,
    ) {
        if set_negative {
            let mask = 1u32 << 31;
            self.psr.set_negative_bit(result & mask == mask);
        }
        if set_zero {
            self.psr.set_zero_bit(result == 0u32);
        }
        if carry.is_some() {
            self.psr.set_carry_or_borrow_bit(carry.unwrap());
        }
        if overflow.is_some() {
            self.psr.set_overflow_bit(overflow.unwrap());
        }
    }

    pub fn sp(&self) -> u32 {
        *self.index(CortexM0Registers::SP_IDX)
    }

    pub fn sp_mut(&mut self) -> &mut u32 {
        self.index_mut(CortexM0Registers::SP_IDX)
    }

    pub fn lr(&self) -> u32 {
        *self.index(CortexM0Registers::LR_IDX)
    }

    pub fn lr_mut(&mut self) -> &mut u32 {
        self.index_mut(CortexM0Registers::LR_IDX)
    }

    pub fn pc(&self) -> u32 {
        self.readable_pc
    }

    pub fn raw_pc(&self) -> u32 {
        self.raw_pc
    }

    pub fn set_pc(&mut self, new_pc: u32) {
        self.raw_pc = new_pc;
        self.readable_pc = new_pc + 4;
    }

    pub fn apsr(&self) -> u32 {
        self.psr.apsr
    }

    pub fn apsr_mut(&mut self) -> &mut u32 {
        &mut self.psr.apsr
    }

    pub fn ipsr(&self) -> u32 {
        self.psr.ipsr
    }

    pub fn ipsr_mut(&mut self) -> &mut u32 {
        &mut self.psr.ipsr
    }

    pub fn epsr(&self) -> u32 {
        self.psr.epsr
    }

    pub fn epsr_mut(&mut self) -> &mut u32 {
        &mut self.psr.epsr
    }

    pub fn control_priv_bit(&self) -> bool {
        get_bit_at_pos(self.control, 0)
    }

    pub fn set_control_priv_bit(&mut self, value: bool) {
        self.control = set_bit_at_pos(self.control, 0, value);
    }

    pub fn control_spsel_bit(&self) -> bool {
        get_bit_at_pos(self.control, 1)
    }

    pub fn set_control_spsel_bit(&mut self, value: bool) {
        self.control = set_bit_at_pos(self.control, 1, value);
    }

    pub fn is_privileged(&self) -> bool {
        self.current_mode == ExecutionMode::Handler || !self.control_priv_bit()
    }
}
