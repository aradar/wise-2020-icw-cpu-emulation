use std::cell::RefCell;
use std::fmt::Debug;
use std::rc::Rc;

use crate::peripheral::error::{ErrorKind, ErrorOrigin, PeripheralError};
use crate::peripheral::AddressBus;

// todo: is the hard req for debug still needed?
pub trait MemoryMappedDevice: Debug {
    fn is_mapped(&self, address: u32) -> bool;

    fn read(&self, address: u32) -> Result<u32, PeripheralError>;
    fn write(&mut self, address: u32, value: u32) -> Result<(), PeripheralError>;
    fn reset(&mut self) {}

    fn read_u8(&self, address: u32) -> Result<u8, PeripheralError> {
        // todo: find a better solution for param origin
        Err(PeripheralError::new_read(
            ErrorKind::NotSupportedOperation,
            address,
            ErrorOrigin::Device("MemoryMappedDevice Trait".to_string()),
        ))
    }

    fn read_u16(&self, address: u32) -> Result<u16, PeripheralError> {
        // todo: find a better solution for param origin
        Err(PeripheralError::new_read(
            ErrorKind::NotSupportedOperation,
            address,
            ErrorOrigin::Device("MemoryMappedDevice Trait".to_string()),
        ))
    }

    fn write_u8(&mut self, address: u32, value: u8) -> Result<(), PeripheralError> {
        // todo: find a better solution for param origin
        Err(PeripheralError::new_write(
            ErrorKind::NotSupportedOperation,
            address,
            ErrorOrigin::Device("MemoryMappedDevice Trait".to_string()),
        ))
    }

    fn write_u16(&mut self, address: u32, value: u16) -> Result<(), PeripheralError> {
        // todo: find a better solution for param origin
        Err(PeripheralError::new_write(
            ErrorKind::NotSupportedOperation,
            address,
            ErrorOrigin::Device("MemoryMappedDevice Trait".to_string()),
        ))
    }
}

pub trait ClockedDevice {
    fn tick(&mut self, bus: Rc<RefCell<AddressBus>>);
}
