use std::cell::RefCell;
use std::rc::Rc;

use crate::peripheral::error::{ErrorKind, ErrorOrigin, PeripheralError};
use crate::peripheral::MemoryMappedDevice;

pub struct AddressBus {
    devices: Vec<Rc<RefCell<dyn MemoryMappedDevice>>>,
}

impl AddressBus {
    pub fn new(devices: Vec<Rc<RefCell<dyn MemoryMappedDevice>>>) -> AddressBus {
        AddressBus { devices }
    }

    fn find_device(&self, address: u32) -> Option<&Rc<RefCell<dyn MemoryMappedDevice>>> {
        self.devices.iter().find(|&d| match d.try_borrow() {
            Ok(d) => d.is_mapped(address),
            Err(_) => false,
        })
    }

    pub fn is_mapped(&self, address: u32) -> bool {
        match self.find_device(address) {
            None => false,
            Some(r) => r.borrow().is_mapped(address),
        }
    }

    pub fn read(&self, address: u32) -> Result<u32, PeripheralError> {
        match self.find_device(address) {
            // todo: this error can be raised if multiple devices request things in a circle
            None => Result::Err(PeripheralError::new_read(
                ErrorKind::UnhandledAddress,
                address,
                ErrorOrigin::AddressDecoder,
            )),
            Some(r) => r.borrow().read(address),
        }
    }

    pub fn read_u8(&self, address: u32) -> Result<u8, PeripheralError> {
        match self.find_device(address) {
            // todo: this error can be raised if multiple devices request things in a circle
            None => Result::Err(PeripheralError::new_read(
                ErrorKind::UnhandledAddress,
                address,
                ErrorOrigin::AddressDecoder,
            )),
            Some(r) => r.borrow().read_u8(address),
        }
    }

    pub fn read_u16(&self, address: u32) -> Result<u16, PeripheralError> {
        match self.find_device(address) {
            // todo: this error can be raised if multiple devices request things in a circle
            None => Result::Err(PeripheralError::new_read(
                ErrorKind::UnhandledAddress,
                address,
                ErrorOrigin::AddressDecoder,
            )),
            Some(r) => r.borrow().read_u16(address),
        }
    }

    pub fn write(&mut self, address: u32, value: u32) -> Result<(), PeripheralError> {
        match self.find_device(address) {
            // todo: this error can be raised if multiple devices request things in a circle
            None => Result::Err(PeripheralError::new_write(
                ErrorKind::UnhandledAddress,
                address,
                ErrorOrigin::AddressDecoder,
            )),
            Some(r) => r.borrow_mut().write(address, value),
        }
    }

    pub fn write_u8(&mut self, address: u32, value: u8) -> Result<(), PeripheralError> {
        match self.find_device(address) {
            // todo: this error can be raised if multiple devices request things in a circle
            None => Result::Err(PeripheralError::new_write(
                ErrorKind::UnhandledAddress,
                address,
                ErrorOrigin::AddressDecoder,
            )),
            Some(r) => r.borrow_mut().write_u8(address, value),
        }
    }

    pub fn write_u16(&mut self, address: u32, value: u16) -> Result<(), PeripheralError> {
        match self.find_device(address) {
            // todo: this error can be raised if multiple devices request things in a circle
            None => Result::Err(PeripheralError::new_write(
                ErrorKind::UnhandledAddress,
                address,
                ErrorOrigin::AddressDecoder,
            )),
            Some(r) => r.borrow_mut().write_u16(address, value),
        }
    }
}
