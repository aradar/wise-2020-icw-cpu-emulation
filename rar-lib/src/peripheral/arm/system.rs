use std::fmt::Debug;

use crate::peripheral::MemoryMappedDevice;

use rusty_arm_macro::MemoryMappedDevice;

// todo: how to handle reset? attribute to mark a reset func?

#[derive(MemoryMappedDevice, Debug)]
pub struct SystemControlBlockDevice {
    /// CPUID Register
    #[register(address = 0xE000ED00, reset = 0x410CC200)]
    cpuid: u32,

    /// Interrupt Control and State Register
    ///
    /// | bits  | name        | md | w_mask | r_mask |
    /// |-------|-------------|----|--------| -------|
    /// | 31    | NMIPENDSET  | RW | 1      | 1      |
    /// | 30:29 |             |    | 00     | 00     |
    /// | 28    | PENDSVSET   | RW | 1      | 1      |
    /// | 27    | PENDSVCLR   | WO | 1      | 0      |
    /// | 26    | PENDSTSET   | RW | 1      | 1      |
    /// | 25    | PENDSTCLR   | WO | 1      | 0      |
    /// | 24:23 |             |    | 00     | 00     |
    /// | 22    | ISRPENDING  | RO | 0      | 1      |
    /// | 21:18 |             |    | 0000   | 0000   |
    /// | 17:12 | VECTPENDING | RO | 000000 | 111111 |
    /// | 11:6  |             |    | 000000 | 000000 |
    /// | 5:0   | VECTACTIVE  | RO | 000000 | 111111 |
    #[register(
        address = 0xE000ED04,
        write_mask = 0b1_00_1_1_1_1_00_0_0000_000000_000000_000000,
        read_mask = 0b1_00_1_0_1_0_00_1_0000_111111_000000_111111
    )]
    icsr: u32,

    /// Application Interrupt and Reset Control Register
    ///
    /// | bits   | name                    | md | w_mask           | r_mask           |
    /// |--------|-------------------------|----|------------------|------------------|
    /// | 31:16  | Reserved (read)         | RW | 0000101111110100 | 0000000000000000 |
    /// |        | VECTKEY (write, 0x05FA) |    |                  |                  |
    /// | 15     | ENDIANESS               | RO | 0                | 1                |
    /// | 14:3   |                         |    | 000000000000     | 000000000000     |
    /// | 2      | SYSRESETREQ             | WO | 1                | 0                |
    /// | 1      | VECTCLRACTIVE           | WO | 1                | 0                |
    /// | 0      |                         |    | 0                | 0                |
    #[register(
        address = 0xE000ED0C,
        write_mask = 0b0000101111110100_0_000000000000_1_1_0,
        read_mask = 0b0000000000000000_1_000000000000_0_0_0,
        reset = 0xFA050000
    )]
    aircr: u32,

    /// System Control Register
    ///
    /// | bits    | name        | md | w_mask                      | r_mask                      |
    /// |---------|-------------|----|-----------------------------|-----------------------------|
    /// | 31:5    |             |    | 000000000000000000000000000 | 000000000000000000000000000 |
    /// | 4       | SEVONPEND   | RW | 1                           | 1                           |
    /// | 3       |             |    | 0                           | 0                           |
    /// | 2       | SLEEPDEEP   | RW | 1                           | 1                           |
    /// | 1       | SLEEPONEXIT | RW | 1                           | 1                           |
    /// | 0       |             |    | 0                           | 0                           |
    #[register(
        address = 0xE000ED10,
        write_mask = 0b000000000000000000000000000_1_0_1_1_0,
        read_mask = 0b000000000000000000000000000_1_0_1_1_0
    )]
    scr: u32,

    /// Configuration and Control Register
    ///
    /// | bits    | name        | md | w_mask                 | r_mask                 |
    /// |---------|-------------|----|------------------------|------------------------|
    /// | 31:10   |             |    | 0000000000000000000000 | 0000000000000000000000 |
    /// | 9       | STKALIGN    | RW | 1                      | 1                      |
    /// | 8:4     |             |    | 00000                  | 00000                  |
    /// | 3       | UNALIGN_TRP | RW | 1                      | 1                      |
    /// | 2:0     |             |    | 000                    | 000                    |
    #[register(
        address = 0xE000ED14,
        write_mask = 0b0000000000000000000000_1_00000_1_000,
        read_mask = 0b0000000000000000000000_1_00000_1_000,
        reset = 0x00000208
    )]
    ccr: u32,

    /// System Handler Priority Register 2
    ///
    /// | bits    | name   | md | w_mask                   | r_mask                   |
    /// |---------|--------|----|--------------------------|--------------------------|
    /// | 31:24   | PRI_11 | RW | 11111111                 | 11111111                 |
    /// | 23:0    |        |    | 000000000000000000000000 | 000000000000000000000000 |
    #[register(
        address = 0xE000ED1C,
        write_mask = 0b11111111_000000000000000000000000,
        read_mask = 0b11111111_000000000000000000000000
    )]
    shpr2: u32,

    /// System Handler Priority Register 3
    ///
    /// | bits    | name   | md | w_mask           | r_mask           |
    /// |---------|--------|----|------------------|------------------|
    /// | 31:24   | PRI_15 | RW | 11111111         | 11111111         |
    /// | 23:16   | PRI_14 | RW | 00000000         | 00000000         |
    /// | 15:0    |        |    | 0000000000000000 | 0000000000000000 |
    #[register(
        address = 0xE000ED20,
        write_mask = 0b11111111_00000000_0000000000000000,
        read_mask = 0b11111111_00000000_0000000000000000
    )]
    shpr3: u32,
}

impl SystemControlBlockDevice {
    #[allow(dead_code)]
    pub fn new() -> SystemControlBlockDevice {
        SystemControlBlockDevice {
            // sets IMPLEMENTER to 0x41 (ARM Limited), ARCHITECTURE to 0xC
            // (ARMv6-M), PARTNO to 0xC20 (Cortex-M0) and VARIANT and REVISION to 0
            cpuid: 0x410CC200,
            icsr: 0x0,
            aircr: 0xFA050000,
            scr: 0x0,
            ccr: 0x00000208,
            shpr2: 0x0,
            shpr3: 0x0,
        }
    }
}
