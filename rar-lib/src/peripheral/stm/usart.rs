use std::cell::RefCell;
use std::collections::VecDeque;
use std::rc::Rc;

use crate::peripheral::error::{ErrorKind, ErrorOrigin, PeripheralError};
use crate::peripheral::{AddressBus, ClockedDevice, MemoryMappedDevice};
use crate::utils::{get_bit_at_pos, get_bits_at_pos, set_bits_at_pos};

// https://knowledge.ni.com/KnowledgeArticleDetails?id=kA00Z0000019M5ASAU&l=de-DE
// queue length needs to be configurable

// https://www.st.com/resource/en/reference_manual/dm00091010-stm32f030x4x6x8xc-and-stm32f070x6xb-advanced-armbased-32bit-mcus-stmicroelectronics.pdf

// todo: build a generic uart implementation and only a small wrapper for
//  specific providers with there address spaces

const CR1_OFFSET: u32 = 0x0;
const CR2_OFFSET: u32 = 0x4;
const CR3_OFFSET: u32 = 0x8;
const BRR_OFFSET: u32 = 0x0C;
const RTOR_OFFSET: u32 = 0x14;
const RQR_OFFSET: u32 = 0x18;
const ISR_OFFSET: u32 = 0x1C;
const ICR_OFFSET: u32 = 0x20;
const RDR_OFFSET: u32 = 0x24;
const TDR_OFFSET: u32 = 0x28;

#[derive(Debug)]
pub struct Usart {
    base_address: u32,
    cr1: u32,
    cr2: u32,
    cr3: u32,
    brr: u32,
    rtor: u32,
    rqr: u32,
    isr: u32,
    icr: u32,
    rdr: u32,
    tdr: u32,
    receive_queue: VecDeque<u32>,
    transmit_queue: VecDeque<u32>,
}

impl ClockedDevice for Usart {
    fn tick(&mut self, _bus: Rc<RefCell<AddressBus>>) {
        // todo: implements only polling mode

        // todo: reading from rdr does not reset the RXNE field! this needs to be done in software

        if !self.UE() {
            self.isr = 0xC0;
            return;
        }

        if self.RE() {
            if !self.RXNE() && !self.receive_queue.is_empty() {
                self.rdr = self.receive_queue.pop_front().unwrap();
                self.set_RXNE(u32::MAX);

                if !self.receive_queue.is_empty() {
                    self.set_ORE(u32::MAX);
                }
            }
        }

        if self.TE() {
            if !self.TXE() {
                self.transmit_queue.push_back(self.tdr);
                self.set_TXE(u32::MAX);
                self.set_TC(u32::MAX);

                println!(
                    "Added a new value to the transmit queue. It currently holds: {}",
                    self.transmit_queue.iter().map(|x| std::char::from_u32(*x).unwrap_or(' ')).collect::<String>()
                )
            }
        }

        // UE
        // When this bit is cleared, the USART prescalers and outputs are stopped immediately, and
        // current operations are discarded. The configuration of the USART is kept, but all the
        // status flags, in the USART_ISR are set to their default values. This bit is set and
        // cleared by software.

        // what is DMA??
        // different transfer mode. data gets directly written into memory and needs no direct
        // interaction of the cpu
    }
}

impl MemoryMappedDevice for Usart {
    fn is_mapped(&self, address: u32) -> bool {
        if address < self.base_address {
            false
        } else {
            match address - self.base_address {
                CR1_OFFSET | CR2_OFFSET | CR3_OFFSET | BRR_OFFSET | RTOR_OFFSET | RQR_OFFSET
                | ISR_OFFSET | ICR_OFFSET | RDR_OFFSET | TDR_OFFSET => true,
                _ => false,
            }
        }
    }

    fn read(&self, address: u32) -> Result<u32, PeripheralError> {
        if !self.is_mapped(address) {
            Err(PeripheralError::new_read(
                ErrorKind::UnhandledAddress,
                address,
                ErrorOrigin::Device("Usart".to_string()),
            ))
        } else {
            match address - self.base_address {
                CR1_OFFSET => Ok(self.cr1),
                CR2_OFFSET => Ok(self.cr2),
                CR3_OFFSET => Ok(self.cr3),
                BRR_OFFSET => Ok(self.brr),
                RTOR_OFFSET => Ok(self.rtor),
                RQR_OFFSET => Ok(self.rqr),
                ISR_OFFSET => Ok(self.isr),
                ICR_OFFSET => Ok(self.icr),
                // Reading the rdr register should trigger a change of the RXNE bitfield, but this
                // is not possible as the API for the BUS requires the read function to be not
                // mutable. Therefore the RXNE bitfield needs to be cleared manually by software :(
                RDR_OFFSET => Ok(self.rdr),
                TDR_OFFSET => Ok(self.tdr),
                _ => unreachable!(),
            }
        }
    }

    fn write(&mut self, address: u32, value: u32) -> Result<(), PeripheralError> {
        if !self.is_mapped(address) {
            Err(PeripheralError::new_write(
                ErrorKind::UnhandledAddress,
                address,
                ErrorOrigin::Device("Usart".to_string()),
            ))
        } else {
            match address - self.base_address {
                CR1_OFFSET => Ok(self.set_CR1_reg(value)),
                CR2_OFFSET => Ok(self.set_CR2_reg(value)),
                CR3_OFFSET => Ok(self.set_CR3_reg(value)),
                BRR_OFFSET => Ok(self.set_BRR_reg(value)),
                RTOR_OFFSET => Ok(self.set_RTOR_reg(value)),
                RQR_OFFSET => Ok(self.set_RQR_reg(value)),
                ISR_OFFSET => Ok(self.set_ISR_reg(value)),
                ICR_OFFSET => Ok(self.set_ICR_reg(value)),
                RDR_OFFSET => Ok(self.set_RDR_reg(value)),
                TDR_OFFSET => Ok(self.set_TDR_reg(value)),
                _ => unreachable!(),
            }
        }
    }

    fn reset(&mut self) {
        self.cr1 = 0;
        self.cr2 = 0;
        self.cr3 = 0;
        self.brr = 0;
        self.rtor = 0;
        self.rqr = 0;
        // the docu claims 0x020000C0 at one spot and 0xC0 at a other one. 0xC0 has been chosen as
        // the highest bit which would be set by 0x020000C0 is not used in the register anyway
        self.isr = 0xC0;
        self.icr = 0;
        self.rdr = 0;
        self.tdr = 0;
        self.receive_queue.clear();
        self.transmit_queue.clear();
    }
}

#[allow(dead_code, non_snake_case)]
impl Usart {
    pub fn new(base_address: u32) -> Usart {
        Usart {
            base_address,
            cr1: 0,
            cr2: 0,
            cr3: 0,
            brr: 0,
            rtor: 0,
            rqr: 0,
            isr: 0,
            icr: 0,
            rdr: 0,
            tdr: 0,
            receive_queue: VecDeque::new(),
            transmit_queue: VecDeque::new(),
        }
    }

    fn set_CR1_reg(&mut self, value: u32) {
        self.set_UE(value);
        self.set_RE(value);
        self.set_TE(value);
        self.set_IDLEIE(value);
        self.set_RXNEIE(value);
        self.set_TCIE(value);
        self.set_TXEIE(value);
        self.set_PEIE(value);
        self.set_PS(value);
        self.set_PCE(value);
        self.set_WAKE(value);
        self.set_M0(value);
        self.set_MME(value);
        self.set_CMIE(value);
        self.set_OVER8(value);
        self.set_DEDT0(value);
        self.set_DEDT1(value);
        self.set_DEDT2(value);
        self.set_DEDT3(value);
        self.set_DEDT4(value);
        self.set_DEAT0(value);
        self.set_DEAT1(value);
        self.set_DEAT2(value);
        self.set_DEAT3(value);
        self.set_DEAT4(value);
        self.set_RTOIE(value);
        self.set_M1(value);
    }

    fn set_CR2_reg(&mut self, value: u32) {
        self.set_ADDM7(value);
        self.set_LBCL(value);
        self.set_CPHA(value);
        self.set_CPOL(value);
        self.set_CLKEN(value);
        self.set_STOP(value);
        self.set_SWAP(value);
        self.set_RXINV(value);
        self.set_TXINV(value);
        self.set_DATAINV(value);
        self.set_MSBFIRST(value);
        self.set_ABREN(value);
        self.set_ABRMOD0(value);
        self.set_ABRMOD1(value);
        self.set_RTOEN(value);
        self.set_ADD_low(value);
        self.set_ADD_high(value);
    }

    fn set_CR3_reg(&mut self, value: u32) {
        self.set_EIE(value);
        self.set_HDSEL(value);
        self.set_DMAR(value);
        self.set_DMAT(value);
        self.set_RTSE(value);
        self.set_CTSE(value);
        self.set_CTSIE(value);
        self.set_ONEBIT(value);
        self.set_OVRDIS(value);
        self.set_DDRE(value);
        self.set_DEM(value);
        self.set_DEP(value);
    }

    fn set_BRR_reg(&mut self, value: u32) {
        self.set_BRR(value);
    }

    fn set_RTOR_reg(&mut self, value: u32) {
        self.set_RTO(value);
    }

    fn set_RQR_reg(&mut self, value: u32) {
        self.set_ABRRQ(value);
        self.set_SBKRQ(value);
        self.set_MMRQ(value);
        self.set_RXFRQ(value);
    }

    fn set_ISR_reg(&mut self, value: u32) {
        self.set_PE(value);
        self.set_FE(value);
        self.set_NF(value);
        self.set_ORE(value);
        self.set_IDLE(value);
        self.set_RXNE(value);
        self.set_TC(value);
        self.set_TXE(value);
        self.set_CTSIF(value);
        self.set_CTS(value);
        self.set_RTOF(value);
        self.set_ABRE(value);
        self.set_ABRF(value);
        self.set_BUSY(value);
        self.set_CMF(value);
        self.set_SBKF(value);
        self.set_RWU(value);
    }

    fn set_ICR_reg(&mut self, value: u32) {
        self.set_PECF(value);
        self.set_FECF(value);
        self.set_NCF(value);
        self.set_ORECF(value);
        self.set_IDLECF(value);
        self.set_TCCF(value);
        self.set_CTSCF(value);
        self.set_RTOCF(value);
        self.set_CMCF(value);
    }

    fn set_RDR_reg(&mut self, value: u32) {
        self.set_RDR(value);
    }

    fn set_TDR_reg(&mut self, value: u32) {
        self.set_TDR(value);
    }

    // region bitfield helper for cr1
    fn UE(&self) -> bool {
        get_bit_at_pos(self.cr1, 0)
    }

    fn set_UE(&mut self, bits: u32) {
        self.cr1 = set_bits_at_pos(self.cr1, 0, bits, 1);
    }

    fn RE(&self) -> bool {
        get_bit_at_pos(self.cr1, 2)
    }

    fn set_RE(&mut self, bits: u32) {
        self.cr1 = set_bits_at_pos(self.cr1, 2, bits, 1);
    }

    fn TE(&self) -> bool {
        get_bit_at_pos(self.cr1, 3)
    }

    fn set_TE(&mut self, bits: u32) {
        self.cr1 = set_bits_at_pos(self.cr1, 3, bits, 1);
    }

    fn IDLEIE(&self) -> bool {
        get_bit_at_pos(self.cr1, 4)
    }

    fn set_IDLEIE(&mut self, bits: u32) {
        self.cr1 = set_bits_at_pos(self.cr1, 4, bits, 1);
    }

    fn RXNEIE(&self) -> bool {
        get_bit_at_pos(self.cr1, 5)
    }

    fn set_RXNEIE(&mut self, bits: u32) {
        self.cr1 = set_bits_at_pos(self.cr1, 5, bits, 1);
    }

    fn TCIE(&self) -> bool {
        get_bit_at_pos(self.cr1, 6)
    }

    fn set_TCIE(&mut self, bits: u32) {
        self.cr1 = set_bits_at_pos(self.cr1, 6, bits, 1);
    }

    fn TXEIE(&self) -> bool {
        get_bit_at_pos(self.cr1, 7)
    }

    fn set_TXEIE(&mut self, bits: u32) {
        self.cr1 = set_bits_at_pos(self.cr1, 7, bits, 1);
    }

    fn PEIE(&self) -> bool {
        get_bit_at_pos(self.cr1, 8)
    }

    fn set_PEIE(&mut self, bits: u32) {
        self.cr1 = set_bits_at_pos(self.cr1, 8, bits, 1);
    }

    fn PS(&self) -> bool {
        get_bit_at_pos(self.cr1, 9)
    }

    fn set_PS(&mut self, bits: u32) {
        if !self.UE() {
            self.cr1 = set_bits_at_pos(self.cr1, 9, bits, 1);
        }
    }

    fn PCE(&self) -> bool {
        get_bit_at_pos(self.cr1, 10)
    }

    fn set_PCE(&mut self, bits: u32) {
        if !self.UE() {
            self.cr1 = set_bits_at_pos(self.cr1, 10, bits, 1);
        }
    }

    fn WAKE(&self) -> bool {
        get_bit_at_pos(self.cr1, 11)
    }

    fn set_WAKE(&mut self, bits: u32) {
        if !self.UE() {
            self.cr1 = set_bits_at_pos(self.cr1, 11, bits, 1);
        }
    }

    fn M0(&self) -> bool {
        get_bit_at_pos(self.cr1, 12)
    }

    fn set_M0(&mut self, bits: u32) {
        self.cr1 = set_bits_at_pos(self.cr1, 12, bits, 1);
    }

    fn MME(&self) -> bool {
        get_bit_at_pos(self.cr1, 13)
    }

    fn set_MME(&mut self, bits: u32) {
        self.cr1 = set_bits_at_pos(self.cr1, 13, bits, 1);
    }

    fn CMIE(&self) -> bool {
        get_bit_at_pos(self.cr1, 14)
    }

    fn set_CMIE(&mut self, bits: u32) {
        self.cr1 = set_bits_at_pos(self.cr1, 14, bits, 1);
    }

    fn OVER8(&self) -> bool {
        get_bit_at_pos(self.cr1, 15)
    }

    fn set_OVER8(&mut self, bits: u32) {
        if !self.UE() {
            self.cr1 = set_bits_at_pos(self.cr1, 15, bits, 1);
        }
    }

    fn DEDT0(&self) -> bool {
        get_bit_at_pos(self.cr1, 16)
    }

    fn set_DEDT0(&mut self, _bits: u32) {
        // If the Driver Enable feature is not supported, this bit is reserved and must be kept
        // cleared.

        // if !self.UE() {
        //     self.cr1 = set_bits_at_pos(self.cr1, 16, bits, 1);
        // }
    }

    fn DEDT1(&self) -> bool {
        get_bit_at_pos(self.cr1, 17)
    }

    fn set_DEDT1(&mut self, _bits: u32) {
        // If the Driver Enable feature is not supported, this bit is reserved and must be kept
        // cleared.

        // if !self.UE() {
        //     self.cr1 = set_bits_at_pos(self.cr1, 17, bits, 1);
        // }
    }

    fn DEDT2(&self) -> bool {
        get_bit_at_pos(self.cr1, 18)
    }

    fn set_DEDT2(&mut self, _bits: u32) {
        // If the Driver Enable feature is not supported, this bit is reserved and must be kept
        // cleared.

        // if !self.UE() {
        //     self.cr1 = set_bits_at_pos(self.cr1, 18, bits, 1);
        // }
    }

    fn DEDT3(&self) -> bool {
        get_bit_at_pos(self.cr1, 19)
    }

    fn set_DEDT3(&mut self, _bits: u32) {
        // If the Driver Enable feature is not supported, this bit is reserved and must be kept
        // cleared.

        // if !self.UE() {
        //     self.cr1 = set_bits_at_pos(self.cr1, 19, bits, 1);
        // }
    }

    fn DEDT4(&self) -> bool {
        get_bit_at_pos(self.cr1, 20)
    }

    fn set_DEDT4(&mut self, _bits: u32) {
        // If the Driver Enable feature is not supported, this bit is reserved and must be kept
        // cleared.

        // if !self.UE() {
        //     self.cr1 = set_bits_at_pos(self.cr1, 20, bits, 1);
        // }
    }

    fn DEAT0(&self) -> bool {
        get_bit_at_pos(self.cr1, 21)
    }

    fn set_DEAT0(&mut self, _bits: u32) {
        // If the Driver Enable feature is not supported, this bit is reserved and must be kept
        // cleared.

        // if !self.UE() {
        //     self.cr1 = set_bits_at_pos(self.cr1, 21, bits, 1);
        // }
    }

    fn DEAT1(&self) -> bool {
        get_bit_at_pos(self.cr1, 22)
    }

    fn set_DEAT1(&mut self, _bits: u32) {
        // If the Driver Enable feature is not supported, this bit is reserved and must be kept
        // cleared.

        // if !self.UE() {
        //     self.cr1 = set_bits_at_pos(self.cr1, 22, bits, 1);
        // }
    }

    fn DEAT2(&self) -> bool {
        get_bit_at_pos(self.cr1, 23)
    }

    fn set_DEAT2(&mut self, _bits: u32) {
        // If the Driver Enable feature is not supported, this bit is reserved and must be kept
        // cleared.

        // if !self.UE() {
        //     self.cr1 = set_bits_at_pos(self.cr1, 23, bits, 1);
        // }
    }

    fn DEAT3(&self) -> bool {
        get_bit_at_pos(self.cr1, 24)
    }

    fn set_DEAT3(&mut self, _bits: u32) {
        // If the Driver Enable feature is not supported, this bit is reserved and must be kept
        // cleared.

        // if !self.UE() {
        //     self.cr1 = set_bits_at_pos(self.cr1, 24, bits, 1);
        // }
    }

    fn DEAT4(&self) -> bool {
        get_bit_at_pos(self.cr1, 25)
    }

    fn set_DEAT4(&mut self, _bits: u32) {
        // If the Driver Enable feature is not supported, this bit is reserved and must be kept
        // cleared.

        // if !self.UE() {
        //     self.cr1 = set_bits_at_pos(self.cr1, 25, bits, 1);
        // }
    }

    fn RTOIE(&self) -> bool {
        get_bit_at_pos(self.cr1, 26)
    }

    fn set_RTOIE(&mut self, _bits: u32) {
        // If the USART does not support the Receiver timeout feature, this bit is reserved and
        // forced by hardware to '0'

        // self.cr1 = set_bits_at_pos(self.cr1, 26, bits, 1);
    }

    fn M1(&self) -> bool {
        get_bit_at_pos(self.cr1, 28)
    }

    fn set_M1(&mut self, bits: u32) {
        if !self.UE() {
            self.cr1 = set_bits_at_pos(self.cr1, 28, bits, 1);
        }
    }
    // endregion

    // region bitfield helper for cr2
    fn ADDM7(&self) -> bool {
        get_bit_at_pos(self.cr2, 4)
    }

    fn set_ADDM7(&mut self, bits: u32) {
        if !self.UE() {
            self.cr2 = set_bits_at_pos(self.cr2, 4, bits, 1);
        }
    }

    fn LBCL(&self) -> bool {
        get_bit_at_pos(self.cr2, 8)
    }

    fn set_LBCL(&mut self, _bits: u32) {
        // If synchronous mode is not supported, this bit is reserved and forced by hardware to '0'.

        // if !self.UE() {
        //     self.cr2 = set_bits_at_pos(self.cr2, 8, bits, 1);
        // }
    }

    fn CPHA(&self) -> bool {
        get_bit_at_pos(self.cr2, 9)
    }

    fn set_CPHA(&mut self, _bits: u32) {
        // If synchronous mode is not supported, this bit is reserved and forced by hardware to '0'.

        // if !self.UE() {
        //     self.cr2 = set_bits_at_pos(self.cr2, 9, bits, 1);
        // }
    }

    fn CPOL(&self) -> bool {
        get_bit_at_pos(self.cr2, 10)
    }

    fn set_CPOL(&mut self, _bits: u32) {
        // If synchronous mode is not supported, this bit is reserved and forced by hardware to '0'.

        // if !self.UE() {
        //     self.cr2 = set_bits_at_pos(self.cr2, 10, bits, 1);
        // }
    }

    fn CLKEN(&self) -> bool {
        get_bit_at_pos(self.cr2, 11)
    }

    fn set_CLKEN(&mut self, _bits: u32) {
        // If synchronous mode is not supported, this bit is reserved and forced by hardware to '0'.

        // if !self.UE() {
        //     self.cr2 = set_bits_at_pos(self.cr2, 11, bits, 1);
        // }
    }

    fn STOP(&self) -> u32 {
        get_bits_at_pos(self.cr2, 12, 2)
    }

    fn set_STOP(&mut self, bits: u32) {
        if !self.UE() {
            self.cr2 = set_bits_at_pos(self.cr2, 12, bits, 2);
        }
    }

    fn SWAP(&self) -> bool {
        get_bit_at_pos(self.cr2, 15)
    }

    fn set_SWAP(&mut self, bits: u32) {
        if !self.UE() {
            self.cr2 = set_bits_at_pos(self.cr2, 15, bits, 1);
        }
    }

    fn RXINV(&self) -> bool {
        get_bit_at_pos(self.cr2, 16)
    }

    fn set_RXINV(&mut self, bits: u32) {
        if !self.UE() {
            self.cr2 = set_bits_at_pos(self.cr2, 16, bits, 1);
        }
    }

    fn TXINV(&self) -> bool {
        get_bit_at_pos(self.cr2, 17)
    }

    fn set_TXINV(&mut self, bits: u32) {
        if !self.UE() {
            self.cr2 = set_bits_at_pos(self.cr2, 17, bits, 1);
        }
    }

    fn DATAINV(&self) -> bool {
        get_bit_at_pos(self.cr2, 18)
    }

    fn set_DATAINV(&mut self, bits: u32) {
        if !self.UE() {
            self.cr2 = set_bits_at_pos(self.cr2, 18, bits, 1);
        }
    }

    fn MSBFIRST(&self) -> bool {
        get_bit_at_pos(self.cr2, 19)
    }

    fn set_MSBFIRST(&mut self, bits: u32) {
        if !self.UE() {
            self.cr2 = set_bits_at_pos(self.cr2, 19, bits, 1);
        }
    }

    fn ABREN(&self) -> bool {
        get_bit_at_pos(self.cr2, 20)
    }

    fn set_ABREN(&mut self, _bits: u32) {
        // If the USART does not support the auto baud rate feature, this bit is reserved and
        // forced by hardware to '0'

        // self.cr2 = set_bits_at_pos(self.cr2, 20, bits, 1);
    }

    fn ABRMOD0(&self) -> bool {
        get_bit_at_pos(self.cr2, 21)
    }

    fn set_ABRMOD0(&mut self, _bits: u32) {
        // If the USART does not support the auto baud rate feature, this bit is reserved and
        // forced by hardware to '0'

        // if !self.ABREN() || !self.UE() {
        //     self.cr2 = set_bits_at_pos(self.cr2, 21, bits, 1);
        // }
    }

    fn ABRMOD1(&self) -> bool {
        get_bit_at_pos(self.cr2, 22)
    }

    fn set_ABRMOD1(&mut self, _bits: u32) {
        // If the USART does not support the auto baud rate feature, this bit is reserved and
        // forced by hardware to '0'

        // if !self.ABREN() || !self.UE() {
        //     self.cr2 = set_bits_at_pos(self.cr2, 22, bits, 1);
        // }
    }

    fn RTOEN(&self) -> bool {
        get_bit_at_pos(self.cr2, 23)
    }

    fn set_RTOEN(&mut self, _bits: u32) {
        // If the USART does not support the Receiver timeout feature, this bit is reserved and
        // forced by hardware to '0'

        // self.cr2 = set_bits_at_pos(self.cr2, 23, bits, 1);
    }

    fn ADD_low(&self) -> u32 {
        get_bits_at_pos(self.cr2, 24, 4)
    }

    fn set_ADD_low(&mut self, bits: u32) {
        if !self.RE() || !self.UE() {
            self.cr2 = set_bits_at_pos(self.cr2, 24, bits, 4);
        }
    }

    fn ADD_high(&self) -> u32 {
        get_bits_at_pos(self.cr2, 28, 4)
    }

    fn set_ADD_high(&mut self, bits: u32) {
        if !self.RE() || !self.UE() {
            self.cr2 = set_bits_at_pos(self.cr2, 28, bits, 4);
        }
    }
    // endregion

    // region bitfield helper for cr3
    fn EIE(&self) -> bool {
        get_bit_at_pos(self.cr3, 0)
    }

    fn set_EIE(&mut self, bits: u32) {
        self.cr3 = set_bits_at_pos(self.cr3, 0, bits, 1);
    }

    fn HDSEL(&self) -> bool {
        get_bit_at_pos(self.cr3, 3)
    }

    fn set_HDSEL(&mut self, bits: u32) {
        if !self.UE() {
            self.cr3 = set_bits_at_pos(self.cr3, 3, bits, 1);
        }
    }

    fn DMAR(&self) -> bool {
        get_bit_at_pos(self.cr3, 6)
    }

    fn set_DMAR(&mut self, bits: u32) {
        self.cr3 = set_bits_at_pos(self.cr3, 6, bits, 1);
    }

    fn DMAT(&self) -> bool {
        get_bit_at_pos(self.cr3, 7)
    }

    fn set_DMAT(&mut self, bits: u32) {
        self.cr3 = set_bits_at_pos(self.cr3, 7, bits, 1);
    }

    fn RTSE(&self) -> bool {
        get_bit_at_pos(self.cr3, 8)
    }

    fn set_RTSE(&mut self, _bits: u32) {
        // If the hardware flow control feature is not supported, this bit is reserved and forced by
        // hardware to '0'

        // if !self.UE() {
        //     self.cr3 = set_bits_at_pos(self.cr3, 8, bits, 1);
        // }
    }

    fn CTSE(&self) -> bool {
        get_bit_at_pos(self.cr3, 9)
    }

    fn set_CTSE(&mut self, _bits: u32) {
        // If the hardware flow control feature is not supported, this bit is reserved and forced by
        // hardware to '0'

        // if !self.UE() {
        //     self.cr3 = set_bits_at_pos(self.cr3, 9, bits, 1);
        // }
    }

    fn CTSIE(&self) -> bool {
        get_bit_at_pos(self.cr3, 10)
    }

    fn set_CTSIE(&mut self, _bits: u32) {
        // If the hardware flow control feature is not supported, this bit is reserved and forced by
        // hardware to '0'

        // self.cr3 = set_bits_at_pos(self.cr3, 10, bits, 1);
    }

    fn ONEBIT(&self) -> bool {
        get_bit_at_pos(self.cr3, 11)
    }

    fn set_ONEBIT(&mut self, bits: u32) {
        if !self.UE() {
            self.cr3 = set_bits_at_pos(self.cr3, 11, bits, 1);
        }
    }

    fn OVRDIS(&self) -> bool {
        get_bit_at_pos(self.cr3, 12)
    }

    fn set_OVRDIS(&mut self, bits: u32) {
        if !self.UE() {
            self.cr3 = set_bits_at_pos(self.cr3, 12, bits, 1);
            self.set_ORE(0);
        }
    }

    fn DDRE(&self) -> bool {
        get_bit_at_pos(self.cr3, 13)
    }

    fn set_DDRE(&mut self, bits: u32) {
        if !self.UE() {
            self.cr3 = set_bits_at_pos(self.cr3, 13, bits, 1);
        }
    }

    fn DEM(&self) -> bool {
        get_bit_at_pos(self.cr3, 14)
    }

    fn set_DEM(&mut self, _bits: u32) {
        // If the Driver Enable feature is not supported, this bit is reserved and must be kept
        // cleared

        // if !self.UE() {
        //     self.cr3 = set_bits_at_pos(self.cr3, 14, bits, 1);
        // }
    }

    fn DEP(&self) -> bool {
        get_bit_at_pos(self.cr3, 15)
    }

    fn set_DEP(&mut self, _bits: u32) {
        // If the Driver Enable feature is not supported, this bit is reserved and must be kept
        // cleared

        // if !self.UE() {
        //     self.cr3 = set_bits_at_pos(self.cr3, 15, bits, 1);
        // }
    }
    // endregion

    // region bitfield helper for brr
    fn BRR(&self) -> u32 {
        get_bits_at_pos(self.brr, 0, 16)
    }

    fn set_BRR(&mut self, bits: u32) {
        // todo: When OVER8 = 0, BRR[3:0] = USARTDIV[3:0].
        // When OVER8 = 1:
        //   BRR[2:0] = USARTDIV[3:0] shifted 1 bit to the right.
        //   BRR[3] must be kept cleared.

        self.brr = set_bits_at_pos(self.brr, 0, bits, 16);
    }
    // endregion

    // region bitfield helper for rtor
    fn RTO(&self) -> u32 {
        get_bits_at_pos(self.rtor, 0, 24)
    }

    fn set_RTO(&mut self, bits: u32) {
        self.rtor = set_bits_at_pos(self.rtor, 0, bits, 24);
    }
    // endregion

    // region bitfield helper for rqr
    fn set_ABRRQ(&mut self, _bits: u32) {
        // If the USART does not support the auto baud rate feature, this bit is reserved and
        // forced by hardware to '0'.

        // self.rqr = set_bits_at_pos(self.rqr, 0, bits, 1);
    }

    fn set_SBKRQ(&mut self, _bits: u32) {
        // Writing 1 to this bit sets the SBKF flag and request to send a BREAK on the line, as
        // soon as the transmit machine is available.
    }

    fn set_MMRQ(&mut self, bits: u32) {
        if get_bit_at_pos(bits, 2) {
            self.set_RWU(u32::MAX);
            // todo: puts the USART in mute mode
        }
    }

    fn set_RXFRQ(&mut self, bits: u32) {
        if get_bit_at_pos(bits, 3) {
            self.set_RXNE(u32::MAX);
        }
    }
    // endregion

    // region bitfield helper for isr
    fn PE(&self) -> bool {
        get_bit_at_pos(self.isr, 0)
    }

    fn set_PE(&mut self, bits: u32) {
        self.isr = set_bits_at_pos(self.isr, 0, bits, 1);
    }

    fn FE(&self) -> bool {
        get_bit_at_pos(self.isr, 1)
    }

    fn set_FE(&mut self, bits: u32) {
        self.isr = set_bits_at_pos(self.isr, 1, bits, 1);
    }

    fn NF(&self) -> bool {
        get_bit_at_pos(self.isr, 2)
    }

    fn set_NF(&mut self, bits: u32) {
        self.isr = set_bits_at_pos(self.isr, 2, bits, 1);
    }

    fn ORE(&self) -> bool {
        get_bit_at_pos(self.isr, 3)
    }

    fn set_ORE(&mut self, bits: u32) {
        // This bit is permanently forced to 0 (no overrun detection) when the OVRDIS bit is set in
        // the USART_CR3 register
        if self.OVRDIS() {
            self.isr = set_bits_at_pos(self.isr, 3, 0, 1);
        } else {
            self.isr = set_bits_at_pos(self.isr, 3, bits, 1);
        }
    }

    fn IDLE(&self) -> bool {
        get_bit_at_pos(self.isr, 4)
    }

    fn set_IDLE(&mut self, bits: u32) {
        self.isr = set_bits_at_pos(self.isr, 4, bits, 1);
    }

    fn RXNE(&self) -> bool {
        get_bit_at_pos(self.isr, 5)
    }

    fn set_RXNE(&mut self, bits: u32) {
        self.isr = set_bits_at_pos(self.isr, 5, bits, 1);
    }

    fn TC(&self) -> bool {
        get_bit_at_pos(self.isr, 6)
    }

    fn set_TC(&mut self, bits: u32) {
        self.isr = set_bits_at_pos(self.isr, 6, bits, 1);
    }

    fn TXE(&self) -> bool {
        get_bit_at_pos(self.isr, 7)
    }

    fn set_TXE(&mut self, bits: u32) {
        self.isr = set_bits_at_pos(self.isr, 7, bits, 1);
    }

    fn CTSIF(&self) -> bool {
        get_bit_at_pos(self.isr, 9)
    }

    fn set_CTSIF(&mut self, _bits: u32) {
        // If the hardware flow control feature is not supported, this bit is reserved and forced by
        // hardware to '0'
        // self.isr = set_bits_at_pos(self.isr, 9, bits, 1);
    }

    fn CTS(&self) -> bool {
        get_bit_at_pos(self.isr, 10)
    }

    fn set_CTS(&mut self, _bits: u32) {
        // If the hardware flow control feature is not supported, this bit is reserved and forced by
        // hardware to '0'
        // self.isr = set_bits_at_pos(self.isr, 10, bits, 1);
    }

    fn RTOF(&self) -> bool {
        get_bit_at_pos(self.isr, 11)
    }

    fn set_RTOF(&mut self, _bits: u32) {
        // If the USART does not support the Receiver timeout feature, this bit is reserved and
        // forced by hardware to '0'
        // self.isr = set_bits_at_pos(self.isr, 11, bits, 1);
    }

    fn ABRE(&self) -> bool {
        get_bit_at_pos(self.isr, 14)
    }

    fn set_ABRE(&mut self, _bits: u32) {
        // If the USART does not support the auto baud rate feature, this bit is reserved and
        // forced by hardware to '0'.
        // self.isr = set_bits_at_pos(self.isr, 14, bits, 1);
    }

    fn ABRF(&self) -> bool {
        get_bit_at_pos(self.isr, 15)
    }

    fn set_ABRF(&mut self, _bits: u32) {
        // If the USART does not support the auto baud rate feature, this bit is reserved and
        // forced by hardware to '0'.
        // self.isr = set_bits_at_pos(self.isr, 15, bits, 1);
    }

    fn BUSY(&self) -> bool {
        get_bit_at_pos(self.isr, 16)
    }

    fn set_BUSY(&mut self, bits: u32) {
        self.isr = set_bits_at_pos(self.isr, 16, bits, 1);
    }

    fn CMF(&self) -> bool {
        get_bit_at_pos(self.isr, 17)
    }

    fn set_CMF(&mut self, bits: u32) {
        self.isr = set_bits_at_pos(self.isr, 17, bits, 1);
    }

    fn SBKF(&self) -> bool {
        get_bit_at_pos(self.isr, 18)
    }

    fn set_SBKF(&mut self, bits: u32) {
        self.isr = set_bits_at_pos(self.isr, 18, bits, 1);
    }

    fn RWU(&self) -> bool {
        get_bit_at_pos(self.isr, 19)
    }

    fn set_RWU(&mut self, bits: u32) {
        self.isr = set_bits_at_pos(self.isr, 19, bits, 1);
    }
    // endregion

    // region bitfield helper for icr
    fn PECF(&self) -> bool {
        get_bit_at_pos(self.icr, 0)
    }

    fn set_PECF(&mut self, bits: u32) {
        let is_set = get_bit_at_pos(set_bits_at_pos(self.icr, 0, bits, 1), 0);

        if is_set {
            self.set_PE(0);
        }
    }

    fn FECF(&self) -> bool {
        get_bit_at_pos(self.icr, 1)
    }

    fn set_FECF(&mut self, bits: u32) {
        let is_set = get_bit_at_pos(set_bits_at_pos(self.icr, 1, bits, 1), 1);

        if is_set {
            self.set_FE(0);
        }
    }

    fn NCF(&self) -> bool {
        get_bit_at_pos(self.icr, 2)
    }

    fn set_NCF(&mut self, bits: u32) {
        let is_set = get_bit_at_pos(set_bits_at_pos(self.icr, 2, bits, 1), 2);

        if is_set {
            self.set_NF(0);
        }
    }

    fn ORECF(&self) -> bool {
        get_bit_at_pos(self.icr, 3)
    }

    fn set_ORECF(&mut self, bits: u32) {
        let is_set = get_bit_at_pos(set_bits_at_pos(self.icr, 3, bits, 1), 3);

        if is_set {
            self.set_ORE(0);
        }
    }

    fn IDLECF(&self) -> bool {
        get_bit_at_pos(self.icr, 4)
    }

    fn set_IDLECF(&mut self, bits: u32) {
        let is_set = get_bit_at_pos(set_bits_at_pos(self.icr, 4, bits, 1), 4);

        if is_set {
            self.set_IDLE(0);
        }
    }

    fn TCCF(&self) -> bool {
        get_bit_at_pos(self.icr, 6)
    }

    fn set_TCCF(&mut self, bits: u32) {
        let is_set = get_bit_at_pos(set_bits_at_pos(self.icr, 6, bits, 1), 6);

        if is_set {
            self.set_TC(0);
        }
    }

    fn CTSCF(&self) -> bool {
        get_bit_at_pos(self.icr, 9)
    }

    fn set_CTSCF(&mut self, _bits: u32) {
        // If the hardware flow control feature is not supported, this bit is reserved and forced by
        // hardware to '0'
        // let is_set = get_bit_at_pos(
        //     set_bits_at_pos(self.icr, 9, bits, 1),
        //     9);
        //
        // if is_set {
        //     self.set_CTSIF(0);
        // }
    }

    fn RTOCF(&self) -> bool {
        get_bit_at_pos(self.icr, 11)
    }

    fn set_RTOCF(&mut self, _bits: u32) {
        // If the USART does not support the Receiver timeout feature, this bit is reserved and
        // forced by hardware to '0'

        // let is_set = get_bit_at_pos(
        //     set_bits_at_pos(self.icr, 11, bits, 1),
        //     11);
        //
        // if is_set {
        //     self.set_RTOF(0);
        // }
    }

    fn CMCF(&self) -> bool {
        get_bit_at_pos(self.icr, 17)
    }

    fn set_CMCF(&mut self, bits: u32) {
        let is_set = get_bit_at_pos(set_bits_at_pos(self.icr, 17, bits, 1), 17);

        if is_set {
            self.set_CMF(0);
        }
    }
    // endregion

    // region bitfield helper for RDR
    fn set_RDR(&mut self, bits: u32) {
        self.rdr = set_bits_at_pos(0, 0, bits, 8);
        self.set_RXNE(u32::MAX);
    }
    // endregion

    // region bitfield helper for TDR
    fn set_TDR(&mut self, bits: u32) {
        self.tdr = set_bits_at_pos(0, 0, bits, 8);
        self.set_TXE(u32::MIN);
    }
    // endregion
}
