use std::collections::HashMap;
use std::convert::TryInto;
use std::fmt::Debug;
use std::mem::size_of;
use std::ops::Range;

use crate::peripheral::error::{ErrorKind, ErrorOrigin, Operation, PeripheralError};
use crate::peripheral::MemoryMappedDevice;

#[derive(Default, Debug)]
pub struct Memory {
    offsets: HashMap<Range<u32>, u32>,
    memory: Vec<u8>,
}

impl Memory {
    pub fn new(mut ranges: Vec<Range<u32>>) -> Memory {
        ranges.sort_by(|a, b| a.start.cmp(&b.start));
        let mut offsets = HashMap::new();
        let mut needed_mem: u32 = 0;

        for range in ranges.iter() {
            let len = range.len() as u32;
            offsets.insert(range.clone(), range.start - needed_mem);
            needed_mem += len;
        }

        Memory {
            offsets,
            memory: vec![0; needed_mem as usize],
        }
    }

    fn read_bytes<T>(&self, address: u32) -> Result<&[u8], PeripheralError> {
        let num_bytes = size_of::<T>() as u32;
        let offset = self
            .find_offset(address, num_bytes)
            .map_err(|e| e.remap(Operation::Write))?;
        let real_address = (address - offset) as usize;

        Ok(&self.memory[real_address..(real_address + num_bytes as usize)])
    }

    fn write_bytes(&mut self, address: u32, bytes: &[u8]) -> Result<(), PeripheralError> {
        let offset = self
            .find_offset(address, bytes.len() as u32)
            .map_err(|e| e.remap(Operation::Read))?;
        let real_address = (address - offset) as usize;
        for i in 0..bytes.len() {
            self.memory[real_address + i] = bytes[i];
        }

        Ok(())
    }

    fn find_offset(&self, address: u32, num_bytes: u32) -> Result<u32, PeripheralError> {
        for (range, &offset) in self.offsets.iter() {
            if range.contains(&address) {
                if !range.contains(&(address + num_bytes - 1)) {
                    return Err(PeripheralError::new_undefined(
                        ErrorKind::UnhandledAddress,
                        address,
                        ErrorOrigin::MemorySystem,
                    ));
                }
                return Ok(offset);
            }
        }

        Err(PeripheralError::new_undefined(
            ErrorKind::UnhandledAddress,
            address,
            ErrorOrigin::MemorySystem,
        ))
    }
}

impl MemoryMappedDevice for Memory {
    fn is_mapped(&self, address: u32) -> bool {
        // todo: make this check better
        self.find_offset(address, 1).is_ok()
    }

    fn read(&self, address: u32) -> Result<u32, PeripheralError> {
        let value = u32::from_ne_bytes(self.read_bytes::<u32>(address)?.try_into().unwrap());
        Ok(value)
    }

    fn write(&mut self, address: u32, value: u32) -> Result<(), PeripheralError> {
        self.write_bytes(address, &value.to_ne_bytes())
    }

    fn read_u8(&self, address: u32) -> Result<u8, PeripheralError> {
        let value = self.read_bytes::<u8>(address)?[0];
        Ok(value)
    }

    fn read_u16(&self, address: u32) -> Result<u16, PeripheralError> {
        let value = u16::from_ne_bytes(self.read_bytes::<u16>(address)?.try_into().unwrap());
        Ok(value)
    }

    fn write_u8(&mut self, address: u32, value: u8) -> Result<(), PeripheralError> {
        self.write_bytes(address, &value.to_ne_bytes())
    }

    fn write_u16(&mut self, address: u32, value: u16) -> Result<(), PeripheralError> {
        self.write_bytes(address, &value.to_ne_bytes())
    }
}

#[cfg(test)]
mod tests {
    use super::Memory;
    use super::MemoryMappedDevice;

    // todo: readd the device test which where here somewhere else

    fn build_memory() -> Memory {
        Memory::new(vec![0x0..0x1000u32, 0x3000..0x4000])
    }

    // region write and read tests
    #[test]
    fn write_u32_and_read_4_u8s_at_0x200() {
        let mut mem = build_memory();
        mem.write(0x200, 0x80402010).unwrap();
        assert_eq!(mem.read_u8(0x200).unwrap(), 0x10);
        assert_eq!(mem.read_u8(0x201).unwrap(), 0x20);
        assert_eq!(mem.read_u8(0x202).unwrap(), 0x40);
        assert_eq!(mem.read_u8(0x203).unwrap(), 0x80);
    }

    #[test]
    fn write_u32_and_read_2_u16s_at_0x200() {
        let mut mem = build_memory();
        mem.write(0x200, 0x80402010).unwrap();
        assert_eq!(mem.read_u16(0x200).unwrap(), 0x2010);
        assert_eq!(mem.read_u16(0x202).unwrap(), 0x8040);
    }

    #[test]
    fn write_u32_and_read_u32_at_0x200() {
        let mut mem = build_memory();
        mem.write(0x200, 0x80402010).unwrap();
        assert_eq!(mem.read(0x200).unwrap(), 0x80402010);
    }

    #[test]
    fn write_u32_and_read_4_u8s_at_0x3800() {
        let mut mem = build_memory();
        mem.write(0x3800, 0x80402010).unwrap();
        assert_eq!(mem.read_u8(0x3800).unwrap(), 0x10);
        assert_eq!(mem.read_u8(0x3801).unwrap(), 0x20);
        assert_eq!(mem.read_u8(0x3802).unwrap(), 0x40);
        assert_eq!(mem.read_u8(0x3803).unwrap(), 0x80);
    }

    #[test]
    fn write_u32_and_read_2_u16s_at_0x3800() {
        let mut mem = build_memory();
        mem.write(0x3800, 0x80402010).unwrap();
        assert_eq!(mem.read_u16(0x3800).unwrap(), 0x2010);
        assert_eq!(mem.read_u16(0x3802).unwrap(), 0x8040);
    }

    #[test]
    fn write_u32_and_read_u32_at_0x3800() {
        let mut mem = build_memory();
        mem.write(0x3800, 0x80402010).unwrap();
        assert_eq!(mem.read(0x3800).unwrap(), 0x80402010);
    }
    // endregion
}
