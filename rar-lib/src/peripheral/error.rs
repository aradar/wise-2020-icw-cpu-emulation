extern crate derive_more;
use derive_more::{Display, Error};

#[derive(Clone, Debug, PartialEq, Display)]
pub enum Operation {
    Read,
    Write,
    Tick,
    Undefined,
}

#[derive(Clone, Debug, PartialEq, Display)]
pub enum ErrorOrigin {
    MemorySystem,
    Device(String),
    AddressDecoder,
}

#[derive(Clone, Debug, Display)]
pub enum ErrorKind {
    UnhandledAddress,
    NotSupportedOperation,
}

#[derive(Clone, Debug, Display, Error)]
#[display(
    fmt = "PeripheralError {{ operation: {}, kind: {}, address: {:#x}, origin: {} }}",
    operation,
    kind,
    address,
    origin
)]
pub struct PeripheralError {
    operation: Operation,
    kind: ErrorKind,
    address: u32,
    origin: ErrorOrigin,
}

impl PeripheralError {
    pub fn new_read(kind: ErrorKind, address: u32, origin: ErrorOrigin) -> PeripheralError {
        PeripheralError::new(Operation::Read, kind, address, origin)
    }

    pub fn new_write(kind: ErrorKind, address: u32, origin: ErrorOrigin) -> PeripheralError {
        PeripheralError::new(Operation::Write, kind, address, origin)
    }

    pub fn new_undefined(kind: ErrorKind, address: u32, origin: ErrorOrigin) -> PeripheralError {
        PeripheralError::new(Operation::Undefined, kind, address, origin)
    }

    pub fn new(
        operation: Operation,
        kind: ErrorKind,
        address: u32,
        origin: ErrorOrigin,
    ) -> PeripheralError {
        PeripheralError {
            operation,
            kind,
            address,
            origin,
        }
    }

    pub fn operation(&self) -> &Operation {
        &self.operation
    }

    pub fn kind(&self) -> &ErrorKind {
        &self.kind
    }

    pub fn address(&self) -> u32 {
        self.address
    }

    pub fn origin(&self) -> &ErrorOrigin {
        &self.origin
    }

    pub fn remap(&self, operation: Operation) -> PeripheralError {
        PeripheralError::new(
            operation,
            self.kind.clone(),
            self.address,
            self.origin.clone(),
        )
    }
}
