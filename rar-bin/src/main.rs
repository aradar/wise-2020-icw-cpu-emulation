use core::time;
use std::fmt::Debug;
use std::fs::File;
use std::io::Read;
use std::path::Path;
use std::{io, thread};
use std::cell::RefCell;
use std::rc::Rc;

use clap::Clap;

use rusty_arm::{CortexM0Core, Controller, load_program, Memory, MemoryMappedDevice, ClockedDevice, Usart, SystemControlBlockDevice};

fn path_exists(val: &str) -> Result<(), String> {
    let path = Path::new(&val);

    if path.exists() {
        Ok(())
    } else {
        Err(format!("\"{}\" is not a existing file!", val))
    }
}

#[derive(Clap, Debug)]
#[clap()]
struct Opts {
    /// Path to the bin file which Rusty-ARM should load and execute.
    #[clap(validator=path_exists)]
    program_file: String,
    #[clap(short, long, default_value = "0")]
    /// Initial value of the program counter and address to which the program
    /// gets loaded.
    pc: u32,
    /// Size of the program. If 0 the program size gets auto set to the number
    /// of bytes of the PROGRAM_FILE.
    #[clap(long, default_value = "0")]
    program_size: u32,
    /// Size of the stack given in bytes.
    #[clap(short = 's', long, default_value = "5000")]
    stack_size: u32,
    /// Size of the heap given in bytes.
    #[clap(short = 'h', long, default_value = "20000")]
    heap_size: u32,
}

pub fn read_bin_file(file_path: &Path) -> io::Result<Vec<u8>> {
    let mut f = File::open(file_path)?;
    let mut buffer = Vec::new();
    f.read_to_end(&mut buffer).expect("Reading file failed!");
    Ok(buffer)
}

fn main() {
    let opts: Opts = Opts::parse();

    let program_file_path = Path::new(&opts.program_file);

    let mut program_size = opts.program_size;
    if program_size == 0 {
        program_size = program_file_path.metadata().unwrap().len() as u32;
    }

    // setup of the memory
    let program_range = opts.pc..program_size;
    let stack_range = (0x40000000 - opts.stack_size)..0x40000000;
    let heap_range = 0x20000000..(0x20000000 + opts.heap_size);
    let sp = stack_range.end;
    let mut memory = Memory::new(vec![program_range, heap_range, stack_range]);
    load_program(opts.pc, Path::new(&opts.program_file), &mut memory);

    // setup of the cpu core
    let cpu = Rc::new(RefCell::new(CortexM0Core::new(opts.pc, sp)));

    // setup of the other device
    let usart1 = Rc::new(RefCell::new(Usart::new(0x4001_3800u32)));
    let scb = Rc::new(RefCell::new(SystemControlBlockDevice::new()));

    let mut memory_mapped_devices: Vec<Rc<RefCell<dyn MemoryMappedDevice>>> = Vec::new();
    memory_mapped_devices.push(Rc::new(RefCell::new(memory)));
    memory_mapped_devices.push(usart1.clone());
    memory_mapped_devices.push(scb.clone());

    let mut clocked_devices: Vec<Rc<RefCell<dyn ClockedDevice>>> = Vec::new();
    clocked_devices.push(cpu.clone());
    clocked_devices.push(usart1.clone());

    let mut controller = Controller::new(memory_mapped_devices, clocked_devices);

    loop {
        controller.do_tick();
        //thread::sleep(time::Duration::from_millis(50));
    }
}
