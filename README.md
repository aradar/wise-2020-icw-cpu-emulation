# Building the project
The project can be easily build by calling `cargo build [--release]` (use the 
`--release` flag if you want a release build).

# Running the project
Build the project and after it call `cargo run [--release] -- <args for the tool>`. 
The two dashes indicate cargo to pass all further arguments to the project 
executable instead of parsing them. To get available cli arguments just call 
`cargo run [--release] -- --help` for example.

Alternatively it is of course also possible to just cd in to the 
`target/debug` or `target/release` dir and running the `rar` executable 
manually there.

# Help menu
```
rusty-arm 

USAGE:
    rar [OPTIONS] <program-file>

ARGS:
    <program-file>    Path to the bin file which Rusty-ARM should load and execute

FLAGS:
        --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
    -h, --heap-size <heap-size>          Size of the heap given in bytes [default: 20000]
    -p, --pc <pc>
            Initial value of the program counter and address to which the program gets loaded
            [default: 0]

        --program-size <program-size>
            Size of the program. If 0 the program size gets auto set to the number of bytes of the
            PROGRAM_FILE [default: 0]

    -s, --stack-size <stack-size>        Size of the stack given in bytes [default: 5000]
```