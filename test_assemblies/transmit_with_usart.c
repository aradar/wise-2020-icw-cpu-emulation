int main(int argc, char *argv[]) {

    volatile int *USART1 = (volatile char *) 0x40013800;
    volatile int *USART_CR1 = USART1 + 0;
    // volatile char *USART_CR2 = USART1 + 1;
    // volatile char *USART_CR3 = USART1 + 2;
    // volatile char *USART_BRR = USART1 + 3;
    // volatile char *USART_RTOR = USART1 + 5;
    // volatile char *USART_RQR = USART1 + 6;
    volatile int *USART_ISR = USART1 + 7;
    // volatile char *USART_ICR = USART1 + 8;
    // volatile char *USART_RDR = USART1 + 9;
    volatile int *USART_TDR = USART1 + 10;

    // enable USART
    *USART_CR1 |= 0x1;
    // TE
    *USART_CR1 |= 0x4;

    // while (1) {
    //     // check if TXE == 1
    //     if (*USART_ISR == 0x80) {
    //         *USART_TDR = 'w';
    //     }
    // }

    *USART_TDR = 'H';
    *USART_TDR = 'e';
    *USART_TDR = 'l';
    *USART_TDR = 'l';
    *USART_TDR = 'o';
    *USART_TDR = ' ';
    *USART_TDR = 'W';
    *USART_TDR = 'o';
    *USART_TDR = 'r';
    *USART_TDR = 'l';
    *USART_TDR = 'd';
    *USART_TDR = '!';
}
