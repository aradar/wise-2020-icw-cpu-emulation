# General
This directory contains a simple Makefile and small assembly code snippets 
which can be compiled to runnable binary code for ARMv6-M architecture.

# Build examples
Building the examples is as easy as calling `make` in this directory as long 
as `gcc-arm-none-eabi` and `make` are available through the path.

# Assembly investigation commands
- `arm-none-eabi-nm FILE.elf`
  - Prints the symbols and there position in the given `.elf` file
- `arm-none-eabi-objdump -D -bbinary -marm FILE.bin -Mforce-thumb > FILE.RECONSTRUCTED.s`
  - Decompiles the given `.bin` file and writes the assembly code to a file
- `xxd -e [-b] -c 2 FILE.bin`
  - Prints the compiled assembly as 16bit rows with there index either in hex
    or as binary (`-b`) in little endian order

