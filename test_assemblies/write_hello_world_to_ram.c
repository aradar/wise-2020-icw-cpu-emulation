int main(int argc, char *argv[]) {
    // compiler uses relative to pc data loading to get the literal
    volatile char *start = (volatile char *) 0x20001000;

    // compiler uses shifting to build the literal
    //volatile char *start = (volatile char *) 0x20000000;

    *start        = 'H';
    *(start + 1)  = 'e';
    *(start + 2)  = 'l';
    *(start + 3)  = 'l';
    *(start + 4)  = 'o';
    *(start + 5)  = ' ';
    *(start + 6)  = 'W';
    *(start + 7)  = 'o';
    *(start + 8)  = 'r';
    *(start + 9)  = 'l';
    *(start + 10) = 'd';
    *(start + 11) = '!';
}
